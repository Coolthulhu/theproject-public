﻿using UnityEngine;
using UnityEngine.Assertions;

public class CenterOnGameObject : MonoBehaviour, ITracksObject
{
    [SerializeField]
    private Vector3 offset = Vector3.zero;

    private Bounds bounds;

    public GameObject Tracked { get; set; }

    private void Awake()
    {
        var newBounds = new Bounds();
        var renderers = GetComponentsInChildren<Renderer>();
        foreach (var renderer in renderers)
        {
            newBounds.Encapsulate(renderer.bounds);
        }

        bounds = newBounds;
    }

    private void Update()
    {
        Assert.IsNotNull(Tracked);
        // TODO: Global camera
        var camera = Camera.main;
        
        Vector3 screenPos = camera.WorldToScreenPoint(Tracked.transform.position) + offset;
        transform.position = screenPos;
    }
}
