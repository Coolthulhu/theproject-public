﻿using UnityEngine;

namespace Assets.Communication
{
    public class DestroyedMessage
    {
        public GameObject GameObject { get; }

        public DestroyedMessage(GameObject gameObject)
        {
            GameObject = gameObject;
        }
    }
}