﻿using UnityEngine;

namespace Assets.Communication
{
    public class CreatedMessage
    {
        public GameObject GameObject { get; }

        public CreatedMessage(GameObject gameObject)
        {
            GameObject = gameObject;
        }
    }
}