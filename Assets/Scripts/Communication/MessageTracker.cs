﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Assets.Communication
{
    public class MessageTracker : MonoBehaviour
    {
        private Dictionary<Type, object> messageReceiverImplementations = new Dictionary<Type, object>();

        private void Awake()
        {
            messageReceiverImplementations[typeof(CreatedMessage)] = new MessageTrackerImpl<CreatedMessage>();
            messageReceiverImplementations[typeof(DestroyedMessage)] = new MessageTrackerImpl<DestroyedMessage>();
        }

        public void RegisterReceiver<T>(IMessageReceiver<T> messageReceiver)
        {
            var implementation = (MessageTrackerImpl<T>) messageReceiverImplementations[typeof(T)];
            implementation.RegisterReceiver(messageReceiver);
        }

        public void Emit<T>(T message)
        {
            var implementation = (MessageTrackerImpl<T>) messageReceiverImplementations[typeof(T)];
            implementation.Emit(message);
        }

        private class MessageTrackerImpl<T>
        {
            private HashSet<IMessageReceiver<T>> messageReceivers = new HashSet<IMessageReceiver<T>>();

            public void RegisterReceiver(IMessageReceiver<T> messageReceiver)
            {
                Assert.IsNotNull(messageReceiver);
                messageReceivers.Add(messageReceiver);
            }

            public void Emit(T message)
            {
                foreach (var perceiver in messageReceivers)
                {
                    perceiver.Receive(message);
                }
            }
        }
    }
}