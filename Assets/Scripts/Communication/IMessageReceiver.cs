﻿namespace Assets.Communication
{
    public interface IMessageReceiver<MessageType>
    {
        void Receive(MessageType message);
    }
}