﻿using System.Linq;
using Assets.CritterAI;
using UnityEngine;

namespace Assets.Communication
{
    public class EverythingTracker : MonoBehaviour
    {

        [SerializeField]
        private ColonyBrain colonyBrain = null;

        [SerializeField]
        private MessageTracker messageTracker = null;

        [SerializeField]
        private WorldUpdater worldUpdater = null;

        private void Start()
        {
            var spawners = FindObjectsOfType<SpawnPrefab>();
            foreach (var spawner in spawners)
            {
                spawner.ReplaceGameObject();
            }

            var everyComponent = FindObjectsOfType<MonoBehaviour>();
            var updateables = everyComponent.OfType<IWorldUpdateable>().ToArray();
            var dropoffs = FindObjectsOfType<Dropoff>();
            var monsters = FindObjectsOfType<Monster>();

            colonyBrain.Initialize(messageTracker);
            worldUpdater.Initialize(messageTracker, updateables.Except(monsters).ToArray());

            foreach (var dropoff in dropoffs)
            {
                messageTracker.Emit(new CreatedMessage(dropoff.gameObject));
            }

            foreach (var monster in monsters)
            {
                messageTracker.Emit(new CreatedMessage(monster.GameObject));
            }

            foreach (var mouseSelector in FindObjectsOfType<MouseSelector>())
            {
                mouseSelector.Init();
            }
        }
    }
}