﻿using UnityEngine;
using UnityEngine.Assertions;

public class Pickup : MonoBehaviour, IPickable
{
    private IPickable toAdd;

    public int Amount = 1;

    private void Awake()
    {
        toAdd = new AddToInventory();
        Assert.IsTrue(Amount > 0);
    }

    public void OnPickup(IItemPicker picker)
    {
        toAdd.OnPickup(picker);
    }
}
