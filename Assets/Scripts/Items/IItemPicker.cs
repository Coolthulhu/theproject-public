﻿using UnityEngine;

public interface IItemPicker
{
    GameObject GameObject { get; }
    Inventory Inventory { get; }
}