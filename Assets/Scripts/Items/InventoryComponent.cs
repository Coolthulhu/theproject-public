﻿using UnityEngine;
using System.Collections;

public class InventoryComponent : MonoBehaviour, IItemPicker
{
    public Inventory Inventory { get; private set; }

    public GameObject GameObject => gameObject;

    private void Awake()
    {
        Inventory = new Inventory();
    }
}
