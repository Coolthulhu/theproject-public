﻿using System;
using System.Collections;
using System.Collections.Generic;

public class Inventory : IEnumerable<InventoryItem>
{
    private List<InventoryItem> items = new List<InventoryItem>();

    public int Count => items.Count;

    public void Add(InventoryItem item)
    {
        items.Add(item);
    }

    public void Clear()
    {
        items.Clear();
    }

    public IEnumerator<InventoryItem> GetEnumerator()
    {
        return ((IEnumerable<InventoryItem>)items).GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return ((IEnumerable<InventoryItem>)items).GetEnumerator();
    }

    public bool Remove(InventoryItem item)
    {
        return items.Remove(item);
    }
}