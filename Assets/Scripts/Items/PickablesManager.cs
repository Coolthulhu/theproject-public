﻿using System;
using System.Collections.Generic;
using Assets.Communication;
using Assets.CritterAI;
using UnityEngine;
using UnityEngine.Assertions;
using Random = UnityEngine.Random;

[Serializable]
public class PickablesManager : MonoBehaviour, IWorldUpdateable
{
    [SerializeField]
    private List<Pickup> pickups = null;
    [SerializeField]
    private Pickup pickupPrefab = null;
    [SerializeField]
    private MessageTracker messageTracker = null;
    [SerializeField]
    private TerrainManager terrainManager = null;

    [SerializeField]
    private int SpawnCount = 10;
    
    private void Start()
    {
        messageTracker = messageTracker ?? FindObjectOfType<MessageTracker>();
        Assert.IsNotNull(messageTracker);
        terrainManager = terrainManager ?? FindObjectOfType<TerrainManager>();
        Assert.IsNotNull(terrainManager);
    }

    public void WorldUpdate()
    {
        while (pickups.Count < SpawnCount)
        {
            SpawnPickup(null);
        }
    }

    public void Add(Pickup newPickup)
    {
        pickups.Add(newPickup);
        messageTracker.Emit(new CreatedMessage(newPickup.gameObject));
    }

    public void PickupSpecific(IItemPicker picker, Pickup pickup)
    {
        Ensure.NotNull(pickup);
        pickup.OnPickup(picker);
        pickup.Amount--;
        if (pickup.Amount <= 0)
        {
            pickups.Remove(pickup);
            RemoveObject(pickup);
        }
    }

    public void SpawnPickup(InventoryItem item)
    {
        var bounds = terrainManager.Bounds;
        var randomPoint = Random.insideUnitSphere * 0.5f;
        var newPosMap = Vector3.Scale(randomPoint, bounds.size);
        var newPosTerrain = newPosMap;
        var newPickup = Instantiate(pickupPrefab, newPosTerrain, Quaternion.identity);
        newPickup.transform.SetParent(terrainManager.transform, false);
        newPickup.transform.position = terrainManager.LocalToLocalCoords(newPickup.transform.position);
        Add(newPickup);
    }

    private void RemoveObject(Pickup pickup)
    {
        messageTracker.Emit(new DestroyedMessage(pickup.gameObject));
        Destroy(pickup.gameObject);
    }
}