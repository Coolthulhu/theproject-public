﻿public class AddToInventory : IPickable
{
    private InventoryItem item = new InventoryItem();

    public void OnPickup(IItemPicker picker)
    {
        Ensure.NotNull(picker);
        picker.Inventory.Add(item);
    }
}