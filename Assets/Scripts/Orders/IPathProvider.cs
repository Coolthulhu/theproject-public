﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Orders
{
    public interface IPathProvider
    {
        List<Vector3> Path { get; set; }
        void Pop();
    }
}
