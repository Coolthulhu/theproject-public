﻿public interface IOrder<T>
{
    void Apply(T actor);
    void Preview(T actor, ManualQueuePreview manualQueuePreview);
}