﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System;

[RequireComponent(typeof(Selectable))]
public class CommandReceiver : MonoBehaviour
{
    [SerializeField]
    private bool autoPopulate = false;

    [SerializeField]
    private IList<IOrderable> receivers;

    private void Awake()
    {
        if (autoPopulate)
        {
            receivers = GetComponents<IOrderable>();
        }

        if (receivers.Count < 1)
        {
            throw new InvalidDataException();
        }
    }
    
    public IEnumerable<ActionQueue> BuildOrder(UIOrderParams uiOrderParams)
    {
        if (!receivers.Any())
        {
            throw new InvalidDataException();
        }

        return receivers.Select(selectable => selectable.BuildOrder(uiOrderParams));
    }
}
