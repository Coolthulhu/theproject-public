﻿using Assets.Actions;
using UnityEngine;

public delegate void OrderAppliedEventHandler(IOrderable actor, ActionQueue actionQueue);

public interface IOrderable
{
    GameObject GameObject { get; }
    ActionQueue ActionQueue { get; set; }
    ActionQueue BuildOrder(UIOrderParams uiOrderParams);
    event OrderAppliedEventHandler OrderApplying;
}