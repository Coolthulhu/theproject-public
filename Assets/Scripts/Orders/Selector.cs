﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class Selector
{
    [SerializeField]
    private TerrainManager terrainManager;
    
    public Selection Select(IList<Selectable> toSelect)
    {
        return new Selection(toSelect.Select(x => new SelectedItem(x)).ToList());
    }

    public Selection ToggleSelection(Selection selection, IEnumerable<Selectable> toToggle)
    {
        var oldSelected = selection.Select(x => x.Selectable);
        var added = toToggle.Except(oldSelected);
        if (added.Any())
        {
            return AddToSelection(selection, added.ToArray());
        }
        
        return RemoveFromSelection(selection, new HashSet<Selectable>(toToggle));
    }

    public Selection AddToSelection(Selection selection, IEnumerable<Selectable> toAdd)
    {
        Ensure.NotNull(selection);
        var newList = selection.Selected.ToList();
        newList.AddRange(toAdd.Select(x =>
        {
            return new SelectedItem(x);
        }));
        return new Selection(newList);
    }

    public Selection RemoveFromSelection(Selection selection, HashSet<Selectable> toRemove)
    {
        Ensure.NotNull(selection);
        var newList = selection.Selected.Where(x => !toRemove.Contains(x.Selectable));
        return new Selection(newList.ToList());
    }
}