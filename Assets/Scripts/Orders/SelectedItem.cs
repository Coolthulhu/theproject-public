﻿using Assets.Utils;
using UnityEngine;

public class SelectedItem
{
    public Selectable Selectable { get; }

    public CommandReceiver CommandReceiver { get; }

    public SelectedItem(Selectable selectable)
    {
        Ensure.NotNull(selectable);
        Selectable = selectable;
        CommandReceiver = Selectable.CommandReceiver;
    }
}