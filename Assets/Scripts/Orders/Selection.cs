﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Selection : IEnumerable<SelectedItem>
{
    public IReadOnlyCollection<SelectedItem> Selected { get; }

    public Selection(IReadOnlyCollection<SelectedItem> selected)
    {
        Selected = selected;
    }

    public IEnumerable<PreparedUIOrder> BuildOrder(UIOrderParams uiOrderParams)
    {
        IEnumerable<PreparedUIOrder> selectionFun(SelectedItem s)
        {
            if (s.CommandReceiver != null)
            {
                var orders = s.CommandReceiver.BuildOrder(uiOrderParams);
                return orders.Select(o => new PreparedUIOrder(s, o));
            }

            return Array.Empty<PreparedUIOrder>();
        }
        return Selected.SelectMany(selectionFun);
    }

    public IEnumerator<SelectedItem> GetEnumerator()
    {
        return Selected.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return Selected.GetEnumerator();
    }
}