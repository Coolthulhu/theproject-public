﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Actions;
using Assets.CritterAI;
using Assets.Scripts.CritterAI.Actions;
using UnityEngine;

public class ActionQueue : IEnumerable<IActionInstance>
{
    private Queue<IActionInstance> actions;

    public ActionQueue(IOrderable actor)
    {
        Actor = actor;
        actions = new Queue<IActionInstance>();
    }

    public ActionQueue(IOrderable actor, IActionInstance action)
    {
        Actor = actor;
        actions = new Queue<IActionInstance>(new []{action});
    }

    public void Apply()
    {
        Actor.ActionQueue = this;
    }

    public ActionQueue(IOrderable actor, IEnumerable<IActionInstance> actions)
    {
        Actor = actor;
        this.actions = new Queue<IActionInstance>(actions);
    }

    public IOrderable Actor { get; }

    public int Count => actions.Count;
    
    public IEnumerator<IActionInstance> GetEnumerator()
    {
        return actions.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return actions.GetEnumerator();
    }

    public IActionInstance Dequeue()
    {
        return actions.Dequeue();
    }

    public IActionInstance Peek()
    {
        return actions.Count > 0 ? actions.Peek() : null;
    }
}