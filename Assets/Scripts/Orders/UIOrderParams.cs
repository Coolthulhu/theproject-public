﻿using UnityEngine;

public class UIOrderParams
{
    public UIOrderParams(Vector3 point, GameObject targetObject)
    {
        Point = point;
        TargetObject = targetObject;
    }

    public GameObject TargetObject { get; set; }
    public Vector3 Point { get; set; }
}