﻿using UnityEngine;

namespace Assets.Orders
{

    public class ItemTargeter : MonoBehaviour, IItemTargeter
    {
        public GameObject Item { get; set; }
    }
}