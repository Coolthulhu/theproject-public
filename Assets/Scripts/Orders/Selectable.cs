﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Selectable : MonoBehaviour
{
    public CommandReceiver CommandReceiver { get; private set; }

    public IReadOnlyCollection<GameObject> InitialHierarchy { get; private set; }

    public IReadOnlyCollection<Renderer> InitialRenderers { get; private set; }

    private void Awake()
    {
        CommandReceiver = GetComponent<CommandReceiver>();
        InitialHierarchy = GetComponentsInChildren<Transform>().Select(x => x.gameObject).ToArray();
        InitialRenderers = GetComponentsInChildren<Renderer>();
    }
}
