﻿using UnityEngine;
using System.Collections;
using Assets.Orders;
using System.Collections.Generic;

public class OrderHolder : MonoBehaviour, IPathProvider, IItemTargeter
{
    public List<Vector3> Path { get; set; } = new List<Vector3>();
    public GameObject Item { get; set; }

    public void Pop()
    {
        Path.RemoveAt(0);
    }
}
