﻿public class PreparedUIOrder
{
    public PreparedUIOrder(SelectedItem selectedActor, ActionQueue actionQueue)
    {
        this.SelectedActor = selectedActor;
        this.ActionQueue = actionQueue;
    }

    public SelectedItem SelectedActor { get; }

    public ActionQueue ActionQueue { get; }

    public void Apply()
    {
        ActionQueue.Apply();
    }
}