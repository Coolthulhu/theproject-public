﻿using UnityEngine;

namespace Assets.Orders
{
    public interface IItemTargeter
    {
        GameObject Item { get; set; }
    }
}