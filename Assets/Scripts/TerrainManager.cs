﻿using System;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[Serializable]
public class TerrainManager : MonoBehaviour
{
    [SerializeField]
    private int w = 10;

    [SerializeField]
    private int h = 10;

    [SerializeField]
    private Shader debugShader = null;

    [NonSerialized]
    private float[] hmap;

    [NonSerialized]
    private PathfindingCache pathfindingCache;

    [SerializeField]
    private PickablesManager pickablesManager;

    public Bounds Bounds { get; private set; }

    private void Awake()
    {
        var size = new Vector3(w, 0, h);
        var center = size * 0.5f;
        Bounds = new Bounds(center, size);
        Apply();
    }
    
    public void Apply()
    {
        hmap = Enumerable.Range(0, w * h)
            .Select(i => UnityEngine.Random.Range(0.0f, 2.0f))
            .ToArray();

        var meshFilter = GetComponent<MeshFilter>();
        meshFilter.mesh = Meshgen.MeshFromHeightmap(hmap, w, h);
        var meshCollider = GetComponent<MeshCollider>();
        meshCollider.sharedMesh = meshFilter.mesh;

        var meshRenderer = GetComponent<MeshRenderer>();
        var material = new Material(debugShader);
        material.mainTexture = DebugTextureGenerator.PathfindingTextureFromCache(RefreshPathfindingCache());
        meshRenderer.materials = new Material[] { material };
    }

    public Vector3 WorldToLocalCoords(Vector3 p)
    {
        var closest = Bounds.ClosestPoint(transform.InverseTransformPoint(p) + Bounds.center) - Bounds.center;
        return new Vector3(closest.x, HeightAtLocalPoint(closest), closest.z);
    }

    public Vector3 MapToWorldCoords(Vector3 p)
    {
        var flatPos = transform.TransformPoint(p - Bounds.center);
        var y = HeightAtMapPoint(p);
        return new Vector3(flatPos.x, y, flatPos.z);
    }

    public Vector3 WorldToMapCoords(Vector3 p)
    {
        var local = transform.InverseTransformPoint(p) + Bounds.center;
        return Bounds.ClosestPoint(local);
    }

    public Vector3 LocalToLocalCoords(Vector3 p)
    {
        var closest = Bounds.ClosestPoint(p + Bounds.center) - Bounds.center;
        return new Vector3(closest.x, HeightAtLocalPoint(closest), closest.z);
    }

    public float HeightAtMapPoint(Vector3 p)
    {
        float tx = p.x;
        float tz = p.z;

        int floorx = Mathf.FloorToInt(tx);
        int ceilx = Mathf.CeilToInt(tx);
        float paramx = tx - floorx;

        if (floorx < 0 || ceilx >= w)
        {
            return -1.0f;
        }

        int floorz = Mathf.FloorToInt(tz);
        int ceilz = Mathf.CeilToInt(tz);
        float paramz = tz - floorz;

        if (floorz < 0 || ceilz >= h)
        {
            return -1.0f;
        }

        float upper = Mathf.Lerp(hmap[floorx + floorz * w], hmap[ceilx + floorz * w], tx);
        float lower = Mathf.Lerp(hmap[floorx + ceilz * w], hmap[ceilx + ceilz * w], tx);
        float ret = Mathf.Lerp(upper, lower, tz);
        return ret;
    }

    public float HeightAtLocalPoint(Vector3 p)
    {
        var mapCoords = p + Bounds.center;
        Vector3 dv = mapCoords - Bounds.min;
        Vector3 range = Bounds.max - Bounds.min;
        float tx = (dv.x * w) / range.x;
        float tz = (dv.z * h) / range.z;
        return HeightAtMapPoint(new Vector3(tx, 0.0f, tz));
    }

    public PathfindingResult Pathfind(Vector3 fromWorld, Vector3 toWorld)
    {
        var from = WorldToMapCoords(fromWorld);
        var to = WorldToMapCoords(toWorld);
        var result = new AStar(RefreshPathfindingCache()).FindPath(from, to);
        return new PathfindingResult(result.Success, result.Path.Select(v => MapToWorldCoords(v)).ToList());
    }

    private PathfindingCache RefreshPathfindingCache()
    {
        if (pathfindingCache == null)
        {
            pathfindingCache = PathfindingCache.FromHeightmap(hmap, w, h);
        }

        return pathfindingCache;
    }
}
