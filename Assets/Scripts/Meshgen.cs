﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class Meshgen
{
    public static Mesh MeshFromHeightmap(IList<float> hmap, int w, int h)
    {
        Ensure.NotNull(hmap);
        var center = new Vector3((float) w / 2, 1, (float) h / 2);
        var uvScale = new Vector2(1.0f / (w - 1), 1.0f / (h - 1));
        var scale = new Vector3(10, 1, 10);

        var vertices = new List<Vector3>(w * h);
        var uvs = new List<Vector2>(w * h);
        for (int x = 0; x < w; x++)
        {
            for (int y = 0; y < h; y++)
            {
                int index = x + y * w;
                var z = hmap[index];
                var vert = new Vector3(x, z, y);
                vertices.Add(vert - center);

                var uv = new Vector2(x, y);
                uvs.Add(Vector2.Scale(uv, uvScale));


            }
        }

        var triangleIndices = new List<int>(6 * (w - 1) * (h - 1));
        for (int x = 0; x < w - 1; x++)
        {
            for (int y = 0; y < h - 1; y++)
            {
                int index = x + y * w;
                triangleIndices.Add(index);
                triangleIndices.Add(index + 1);
                triangleIndices.Add(index + w);

                triangleIndices.Add(index + 1);
                triangleIndices.Add(index + 1 + w);
                triangleIndices.Add(index + w);


            }
        }

        Mesh newMesh = new Mesh();
        newMesh.SetVertices(vertices);
        newMesh.SetTriangles(triangleIndices, 0);
        newMesh.SetUVs(0, uvs);
        newMesh.RecalculateNormals();
        var calcedNormals = newMesh.normals;
        return newMesh;
    }
}
