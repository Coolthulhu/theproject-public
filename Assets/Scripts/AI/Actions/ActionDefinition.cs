﻿using UnityEngine.Assertions;

namespace Assets.Actions
{
    public class ActionDefinition
    {
        public float WorkRequired { get; }
        public string Name { get; }

        public ActionDefinition(string name, float workRequired)
        {
            Name = name;
            Assert.IsTrue(workRequired > 0.0f);
            WorkRequired = workRequired;
        }
    }
}