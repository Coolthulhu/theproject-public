﻿namespace Assets.Actions
{
    public interface IActionInstance
    {
        float Progress { get; }
        string Name { get; }
        string Animation { get; }
        bool DisplayProgress { get; }

        void Act();
    }
}