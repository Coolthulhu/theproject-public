﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace Assets.Scripts.AI.Decisions
{
    public interface IAINode
    {
    }

    [Serializable]
    public class MonsterAINode : IAINode
    {
        [SerializeField]
        private bool hasPickup;
        [SerializeField]
        private bool didDropoff;
        [SerializeField]
        private bool atPickup;
        [SerializeField]
        private bool atDropoff;
        [NonSerialized]
        private Vector3 position;
        
        public MonsterAINode(Vector3 position, bool hasPickup = false, bool didDropoff = false, bool atPickup = false,
            bool atDropoff = false)
        {
            this.Position = position;
            this.HasPickup = hasPickup;
            this.DidDropoff = didDropoff;
            this.AtPickup = atPickup;
            this.AtDropoff = atDropoff;
        }

        public bool HasPickup
        {
            get { return hasPickup; }
            set { hasPickup = value; }
        }

        public bool DidDropoff
        {
            get { return didDropoff; }
            set { didDropoff = value; }
        }

        public bool AtPickup
        {
            get { return atPickup; }
            set { atPickup = value; }
        }

        public bool AtDropoff
        {
            get { return atDropoff; }
            set { atDropoff = value; }
        }

        public Vector3 Position
        {
            get { return position; }
            set { position = value; }
        }

        public MonsterAINode Clone()
        {
            return (MonsterAINode)MemberwiseClone();
        }

        public override bool Equals(object other)
        {
            return other is MonsterAINode otherNode ? Equals(otherNode) : false;
        }

        public bool Equals(MonsterAINode other)
        {
            return HasPickup == other.HasPickup &&
                   DidDropoff == other.DidDropoff &&
                   AtPickup == other.AtPickup &&
                   AtDropoff == other.AtDropoff &&
                   Position.x == other.Position.x &&
                   Position.y == other.Position.y &&
                   Position.z == other.Position.z;
        }

        public override int GetHashCode()
        {
            return (HasPickup ? 1 : 0) +
                   (DidDropoff ? 2 : 0) +
                   (AtDropoff ? 4 : 0) +
                   (AtPickup ? 8 : 0) +
                   Position.GetHashCode();
        }
    }
}