﻿public class StateMachineInstance<TMachineState>
{
    private readonly StateMachine<TMachineState> stateMachine;

    public StateMachineInstance(StateMachine<TMachineState> stateMachine, string initialStateName, TMachineState machineState)
    {
        this.stateMachine = stateMachine;
        MachineState = machineState;
        State = stateMachine.GetState(initialStateName);
    }

    public IState<TMachineState> State { get; private set; }
    public TMachineState MachineState { get; private set; }

    public void ExecuteTransition(string signal)
    {
        var nextState = State.Next(signal, MachineState);
        if (nextState != State)
        {
            var postExit = State.OnExit(signal, MachineState);
            MachineState = nextState.OnEnter(signal, postExit);
        }

        State = nextState;
    }

    public void Run()
    {
        ExecuteTransition("tick");
        MachineState = State.Run(MachineState);
    }
}