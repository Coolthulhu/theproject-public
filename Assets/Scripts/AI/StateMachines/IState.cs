﻿public interface IState<InputType>
{
    IState<InputType> Next(string signal, InputType input);
    InputType OnExit(string signal, InputType input);
    InputType OnEnter(string signal, InputType input);
    InputType Run(InputType machineState);
}