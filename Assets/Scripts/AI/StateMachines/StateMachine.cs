﻿using System.Collections.Generic;
using UnityEngine.Assertions;

// TODO: Split off builder
public class StateMachine<MachineState>
{
    private Dictionary<string, IState<MachineState>> states = new Dictionary<string, IState<MachineState>>();
    
    public StateMachineInstance<MachineState> CreateInstance(string initialState, MachineState machineState)
    {
        return new StateMachineInstance<MachineState>(this, initialState, machineState);
    }

    public IState<MachineState> GetState(string stateName)
    {
        bool got = states.TryGetValue(stateName, out IState<MachineState> state);
        Assert.IsTrue(got);
        return state;
    }

    public void AddState(IState<MachineState> state)
    {
        string name = state.GetType().Name;
        AddState(state, name);
    }

    public void AddState(IState<MachineState> state, string name)
    {
        Assert.IsFalse(states.ContainsKey(name));
        states[name] = state;
    }
}
