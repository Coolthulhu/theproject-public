﻿public class NoopTransitionFunction<MachineState> : ITransitionFunction<MachineState>
{
    public MachineState Call(MachineState machineState)
    {
        return machineState;
    }

    public bool Check(MachineState machineState)
    {
        return true;
    }
}