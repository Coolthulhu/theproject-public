﻿using System;

namespace Assets.StateMachines.PredefinedTransitions
{
    public class SimpleFunctionTransition<MachineState> : ITransitionFunction<MachineState>
    {
        private readonly Func<MachineState, MachineState> function;

        public SimpleFunctionTransition(Func<MachineState, MachineState> function)
        {
            this.function = function;
        }

        public SimpleFunctionTransition(Action<MachineState> function)
        {
            this.function = x =>
            {
                function(x);
                return x;
            };
        }

        public MachineState Call(MachineState machineState)
        {
            return function(machineState);
        }

        public bool Check(MachineState machineState)
        {
            return true;
        }
    }
}