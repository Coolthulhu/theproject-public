﻿public interface ITransitionFunction<MachineState>
{
    MachineState Call(MachineState machineState);
    bool Check(MachineState machineState);
}