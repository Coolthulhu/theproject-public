﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Communication;
using Assets.CritterAI;
using UnityEngine;
using Utils.Extensions;

public class WorldUpdater : MonoBehaviour, IMessageReceiver<CreatedMessage>, IMessageReceiver<DestroyedMessage>
{
    private const float MaxSpeed = 10.0f;
    
    private HashSet<IWorldUpdateable> updateables = null;

    [SerializeField, Range(0.0f, MaxSpeed)]
    private float gameSpeed = 1.0f;

    private float accumulatedProgress = 0.0f;

    public float GameSpeed
    {
        get => gameSpeed;
        set { gameSpeed = Math.Min(MaxSpeed, Math.Max(0.0f, value)); }
    }

    public void Receive(CreatedMessage message)
    {
        var updateable = message.GameObject.GetComponent<IWorldUpdateable>();
        if (updateable != null)
        {
            updateables.Add(updateable);
        }
    }

    public void Receive(DestroyedMessage message)
    {
        var updateable = message.GameObject.GetComponent<IWorldUpdateable>();
        if (updateable != null)
        {
            updateables.Remove(updateable);
        }
    }

    public void Initialize(MessageTracker messageTracker, IEnumerable<IWorldUpdateable> initialUpdateables)
    {
        messageTracker.RegisterReceiver<CreatedMessage>(this);
        messageTracker.RegisterReceiver<DestroyedMessage>(this);

        updateables = new HashSet<IWorldUpdateable>(initialUpdateables);
    }

    private void FixedUpdate()
    {
        accumulatedProgress = Math.Min(2 * MaxSpeed, accumulatedProgress + gameSpeed);
        while (accumulatedProgress > 0.0f)
        {
            accumulatedProgress--;
            foreach (var worldUpdateable in updateables)
            {
                worldUpdateable.WorldUpdate();
            }
        }
    }
}
