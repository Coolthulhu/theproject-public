﻿using UnityEngine;
using UnityEngine.Assertions;

public class BillboardOrienter : MonoBehaviour, ITracksObject
{
    private RectTransform rectTransform;

    public GameObject Tracked { get; set; }

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
    }

    private void Start()
    {
        Assert.IsNotNull(Tracked);
        Update();
    }
    
    private void Update()
    {
        if (!transform.hasChanged)
        {
            return;
        }

        transform.hasChanged = false;

        Vector3 screenPos = Camera.main.WorldToScreenPoint(Tracked.transform.position);
        rectTransform.position = screenPos;
    }
}
