﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Assets.Actions;
using Assets.Communication;
using Assets.CritterAI;
using Assets.Scripts.AI.Decisions;
using Assets.Scripts.CritterAI;
using Assets.Scripts.CritterAI.Actions;
using NUnit.Framework;
using UnityEngine;

namespace Tests
{
    public class BrainTest
    {
        private class TestAINode : IAINode
        {
            public readonly bool done = false;
            public readonly bool condition = false;

            public TestAINode()
            {
            }

            public TestAINode(bool done)
            {
                this.done = done;
            }

            public TestAINode(bool done, bool condition)
            {
                this.done = done;
                this.condition = condition;
            }
        }

        private class TestAIGoal : IAIGoal<TestAINode>
        {
            public float Heuristic(TestAINode node, WorldState worldState)
            {
                return node.done ? 0.0f : 1.0f;
            }

            public bool SatisfiedBy(TestAINode node, WorldState worldState)
            {
                return node.done;
            }
        }

        private class TestAIAction : IAIAction<TestAINode>
        {
            private bool needsCondition = false;

            public TestAIAction(bool needsCondition)
            {
                this.needsCondition = needsCondition;
            }

            public ActionExecutionResult ActionsFromTo(TestAINode from, TestAINode to, IGOAPActor<TestAINode> actor, WorldState worldState)
            {
                throw new NotImplementedException();
            }

            public IEnumerable<AIEdge<TestAINode>> Expand(TestAINode node, IGOAPActor<TestAINode> actor, WorldState worldState)
            {
                return new AIEdge<TestAINode>[]
                {
                    new AIEdge<TestAINode>(new TestAINode(needsCondition || node.condition, true), this, 1.0f), 
                };
            }

            public bool PreconditionsMet(TestAINode node, IGOAPActor<TestAINode> actor, WorldState worldState)
            {
                throw new NotImplementedException();
            }
        }

        private class TestActionInstance : IActionInstance
        {
            public float Progress => default;

            public string Name => default;

            public string Animation => default;

            public bool DisplayProgress => default;

            public void Act()
            {
            }
        }

        private class TestGOAPActor : IGOAPActor<TestAINode>
        {
            public TestGOAPActor(IEnumerable<IAIAction<TestAINode>> actions, IEnumerable<IAIGoal<TestAINode>> goals, PathfindingSettings pathfindingSettings)
            {
                Actions = actions.ToList();
                Goals = goals.ToList();
                PathfindingSettings = pathfindingSettings;
            }

            public IReadOnlyList<IAIAction<TestAINode>> Actions { get; }

            public IReadOnlyList<IAIGoal<TestAINode>> Goals { get; }

            public PathfindingSettings PathfindingSettings { get; }

            public PathfindingResult Pathfind(Vector3 from, Vector3 to)
            {
                throw new NotImplementedException();
            }
        }

        [Test]
        public void PlanEmpty()
        {
            var pathfindingSettings = new PathfindingSettings();
            var goapActor = new TestGOAPActor(new IAIAction<TestAINode>[] {}, new IAIGoal<TestAINode>[] {}, pathfindingSettings);
            var actorState = new TestAINode(false);

            var brain = new ColonyBrainImpl();
            var plan = brain.Plan(goapActor, actorState, brain.GetCurrentWorldState());
            Assert.IsEmpty(plan.Plan);
        }

        [Test]
        public void PlanTrivialGoal()
        {
            var aiGoal = new TestAIGoal();
            var pathfindingSettings = new PathfindingSettings();
            var goapActor = new TestGOAPActor(new IAIAction<TestAINode>[] { }, new IAIGoal<TestAINode>[] { aiGoal }, pathfindingSettings);
            var actorState = new TestAINode(false);

            var brain = new ColonyBrainImpl();
            var plan = brain.Plan(goapActor, actorState, brain.GetCurrentWorldState());
            Assert.IsTrue(plan.Success);
        }

        [Test]
        public void PlanToolUseGoal()
        {
            var aiGoal = new TestAIGoal();
            var pathfindingSettings = new PathfindingSettings();
            var goapActor = new TestGOAPActor(new IAIAction<TestAINode>[] { new TestAIAction(true) }, new IAIGoal<TestAINode>[] { aiGoal }, pathfindingSettings);
            var actorState = new TestAINode(false);

            var brain = new ColonyBrainImpl();
            var plan = brain.Plan(goapActor, actorState, brain.GetCurrentWorldState());
            Assert.That(plan, Has.Some.InstanceOf(typeof(TestActionInstance)));
        }
    }
}
