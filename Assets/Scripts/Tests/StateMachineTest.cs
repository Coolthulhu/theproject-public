﻿using NUnit.Framework;

namespace Tests
{
    public class StateMachineTestBool
    {
        private class TestBoolTransitionFunction : IState<bool?>
        {
            private bool output;

            public static TestBoolTransitionFunction trueState = new TestBoolTransitionFunction(true);
            public static TestBoolTransitionFunction falseState = new TestBoolTransitionFunction(false);

            private TestBoolTransitionFunction(bool output)
            {
                this.output = output;
            }

            public IState<bool?> Next(string signal, bool? input)
            {
                return signal == "true" ? trueState : falseState;
            }

            public bool? OnEnter(string signal, bool? input)
            {
                return output;
            }

            public bool? OnExit(string signal, bool? input)
            {
                return input;
            }

            public bool? Run(bool? machineState)
            {
                return output;
            }
        }

        [Test]
        public void StateMachineBoolTest()
        {
            var stateMachine = new StateMachine<bool?>();
            stateMachine.AddState(TestBoolTransitionFunction.falseState, "false");
            stateMachine.AddState(TestBoolTransitionFunction.trueState, "true");

            bool? gotCalled = false;
            var machineInstance = stateMachine.CreateInstance("false", gotCalled);

            Assert.IsFalse(machineInstance.MachineState);

            machineInstance.ExecuteTransition("true");

            Assert.IsTrue(machineInstance.MachineState);
        }
    }
}
