﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine.Assertions;

public static class Ensure
{
    [Conditional("DEBUG")]
    public static void NotNull(object o)
    {
        if (o == null)
        {
#pragma warning disable CA2208 // Instantiate argument exceptions correctly
            throw new ArgumentNullException();
#pragma warning restore CA2208 // Instantiate argument exceptions correctly
        }
    }

    [Conditional("DEBUG")]
    public static void Contains(object expected, ICollection collection)
    {
        foreach (var obj in collection)
        {
            if (expected.Equals(obj))
            {
                return;
            }
        }

        throw new AssertionException("Collection doesn't contain expected object",
            "Collection doesn't contain expected object");
    }

    [Conditional("DEBUG")]
    public static void NotNull(object o1, object o2)
    {
        NotNull(o1);
        NotNull(o2);
    }

    [Conditional("DEBUG")]
    public static void NotNull(object o1, object o2, object o3)
    {
        NotNull(o1);
        NotNull(o2);
        NotNull(o3);
    }

    [Conditional("DEBUG")]
    public static void NotNull(object o1, object o2, object o3, object o4)
    {
        NotNull(o1);
        NotNull(o2);
        NotNull(o3);
        NotNull(o4);
    }
}
