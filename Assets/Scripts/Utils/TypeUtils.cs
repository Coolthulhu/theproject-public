﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Assets.Utils
{
    public static class TypeUtils
    {
        public static void GetTypedPropertyAccessors<T, Result>(out Dictionary<string, Func<T, Result>> getters, out Dictionary<string, Action<T, Result>> setters)
        {
            var properties = typeof(T).GetProperties().Where(prop => prop.PropertyType == typeof(Result));
            getters = properties.ToDictionary(prop => prop.Name, prop =>
                (Func<T, Result>)Delegate.CreateDelegate(typeof(Func<T, Result>), null, prop.GetGetMethod()));
            setters = properties.ToDictionary(prop => prop.Name, prop =>
                (Action<T, Result>)Delegate.CreateDelegate(typeof(Action<T, Result>), null, prop.GetSetMethod()));
        }
    }
}