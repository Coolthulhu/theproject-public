﻿using UnityEngine;
using UnityEngine.Assertions;

public class SpawnPrefab : MonoBehaviour
{
    [SerializeField]
    private GameObject prefab = null;

    public void ReplaceGameObject()
    {
        Assert.IsNotNull(prefab);
        var newGameObject = Instantiate(prefab, transform);
        newGameObject.transform.SetParent(transform.parent, true);
        Destroy(gameObject);
    }
}
