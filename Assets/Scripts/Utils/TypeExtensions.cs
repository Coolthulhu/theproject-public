﻿using System;
using System.Linq;

namespace Assets.Utils
{
    public static class TypeExtensions
    {
        public static string[] GetFieldNames(this Type thisType)
        {
            return thisType.GetFields().Select(f => f.Name).ToArray();
        }

        public static string[] GetTypedPropertyNames<T>(this Type thisType)
        {
            return thisType.GetProperties().Where(prop => prop.PropertyType == typeof(T)).Select(prop => prop.Name).ToArray();
        }
    }
}