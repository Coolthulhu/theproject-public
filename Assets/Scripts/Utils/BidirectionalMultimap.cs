﻿using System.Collections;
using System.Collections.Generic;

namespace Assets.Utils
{
    public class BidirectionalMultimap<A, B>
    {
        private Dictionary<A, List<B>> forward = new Dictionary<A, List<B>>();
        private Dictionary<B, List<A>> backward = new Dictionary<B, List<A>>();

        public void Add(A from, B to)
        {
            AddOrGet(forward, from, to);
            AddOrGet(backward, to, from);
        }

        private void AddOrGet<L, R>(Dictionary<L, List<R>> dictionary, L from, R to)
        {
            if (dictionary.TryGetValue(from, out var innerList))
            {
                innerList.Add(to);
            }
            else
            {
                dictionary.Add(from, new List<R>() {to});
            }
        }
    }
}