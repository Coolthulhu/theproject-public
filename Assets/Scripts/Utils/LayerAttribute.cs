﻿using UnityEngine;

/// <summary>
/// https://answers.unity.com/questions/609385/type-for-layer-selection.html
/// </summary>
public class LayerAttribute : PropertyAttribute
{
}