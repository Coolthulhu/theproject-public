﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace Assets.Utils
{
    public static class VectorMath
    {
        public static float Distance2DSquared(Vector3 a, Vector3 b)
        {
            return (a.x - b.x) * (a.x - b.x) + (a.z - b.z) * (a.z - b.z);
        }

        // Assumes same local coordinate system!
        public static float Distance2DSquared(GameObject a, GameObject b)
        {
            Vector3 aPos = a.transform.position;
            Vector3 bPos = b.transform.position;
            return (aPos.x - bPos.x) * (aPos.x - bPos.x) + (aPos.z - bPos.z) * (aPos.z - bPos.z);
        }

        public static int IndexOfNearest(Vector3 start, IList<Vector3> destinations)
        {
            Assert.IsTrue(destinations.Count > 0);
            int minIndex = 0;
            float minDist = float.MaxValue;
            for (int i = 0; i < destinations.Count; i++)
            {
                var dist = Distance2DSquared(start, destinations[i]);
                if (dist < minDist)
                {
                    minIndex = i;
                    minDist = dist;
                }
            }

            return minIndex;
        }
    }
}