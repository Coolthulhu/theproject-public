﻿using UnityEngine;

namespace Assets.Utils
{
    public interface IUndoable
    {
        void Do();
        void Undo();
    }
}