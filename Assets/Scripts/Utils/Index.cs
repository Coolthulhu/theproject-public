﻿
using System.Runtime.CompilerServices;

public static class Index
{
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static int Of(int x, int y, int w)
    {
        return x + y * w;
    }
}
