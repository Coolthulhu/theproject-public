﻿using System;
using UnityEngine;
using UnityEngine.Assertions;

namespace Utils.Extensions
{
    public static class ComponentExtensions
    {
        public static T GetComponentNotNull<T>(this Component component) where T : class
        {
            var child = component.GetComponent<T>();
            if (child == null)
            {
                throw new NullReferenceException();
            }

            return child;
        }
    }

    public static class GameObjectExtensions
    {
        public static T GetComponentNotNull<T>(this GameObject gameObject) where T : class
        {
            var child = gameObject.GetComponent<T>();
            if (child == null)
            {
                throw new NullReferenceException();
            }

            return child;
        }
    }
}