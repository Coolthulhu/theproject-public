﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Assets.Orders;
using Utils.Extensions;

public class PathProvider : MonoBehaviour, IPathProvider
{
    public List<Vector3> Path { get; set; } = new List<Vector3>();

    public void Awake()
    {
        if (Path == null)
        {
            Path = new List<Vector3>();
        }
    }

    public void Pop()
    {
        Path.RemoveAt(0);
    }

    public override string ToString()
    {
        return string.Join(", ", Path);
    }
}
