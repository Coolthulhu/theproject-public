﻿using System;
using UnityEngine;
using System.Collections;
using Priority_Queue;
using System.Collections.Generic;
using UnityEngine.Assertions;

public class AStar
{
    private readonly PathfindingCache pathfindingCache;
    private readonly int w;
    private readonly int h;

    public AStar(PathfindingCache pathfindingCache)
    {
        Ensure.NotNull(pathfindingCache);
        this.pathfindingCache = pathfindingCache;
        this.w = pathfindingCache.W;
        this.h = pathfindingCache.H;
    }

    public PathfindingResult FindPath(Vector3 from, Vector3 to)
    {
        var nodeStates = new PathfindingNodeState[w * h];
        var queueNodes = new Pathfinding3DQueueNode[w * h];
        var distances = new float[w * h];
        var parents = new Vector3Int[w * h];

        var queue = new FastPriorityQueue<Pathfinding3DQueueNode>(maxNodes: w * h);
        var fromRounded = RoundVector(from);
        var fromNode = new Pathfinding3DQueueNode(fromRounded);
        int fromIndex = Index(fromRounded);

        var toRounded = RoundVector(to);
        int toIndex = Index(toRounded);

        if (!InBounds(fromIndex) || !InBounds(toIndex))
        {
            return new PathfindingResult();
        }

        if (pathfindingCache.Costs[fromIndex] < 0.0f || pathfindingCache.Costs[toIndex] < 0.0f)
        {
            return new PathfindingResult();
        }

        queueNodes[fromRounded.x + fromRounded.z * w] = fromNode;
        nodeStates[fromIndex] = PathfindingNodeState.Open;
        queue.Enqueue(fromNode, 0.0f);

        while (queue.Count > 0)
        {
            var curNode = queue.Dequeue();
            int index = Index(curNode.pos);
            Assert.IsTrue(InBounds(index));
            Assert.AreEqual(PathfindingNodeState.Open, nodeStates[index]);
            nodeStates[index] = PathfindingNodeState.Closed;

            if (index == toIndex)
            {
                break;
            }

            float curDistance = distances[index];

            int minX = Math.Max(0, curNode.pos.x - 1);
            int maxX = Math.Min(w - 1, curNode.pos.x + 1);
            int minZ = Math.Max(0, curNode.pos.z - 1);
            int maxZ = Math.Min(h - 1, curNode.pos.z + 1);
            for (int x = minX; x <= maxX; x++)
            {
                for (int z = minZ; z <= maxZ; z++)
                {
                    int indexNew = x + z * w;
                    Assert.IsTrue(InBounds(indexNew));
                    float moveCostBase = pathfindingCache.Costs[indexNew];
                    if (moveCostBase < 0.0f)
                    {
                        nodeStates[indexNew] = PathfindingNodeState.Closed;
                        continue;
                    }

                    int y = Mathf.RoundToInt(pathfindingCache.Hmap[indexNew]);
                    var newPoint = new Vector3(x, y, z);

                    float moveCostMult = (x == curNode.pos.x || z == curNode.pos.z) ? 1.0f : 1.41f;
                    float moveCost = moveCostBase * moveCostMult;
                    var newDistance = curDistance + moveCost;
                    float newPriority = newDistance + Heuristic(newPoint, to);
                    switch (nodeStates[indexNew])
                    {
                        case PathfindingNodeState.Closed:
                            continue;
                        case PathfindingNodeState.Open:
                            if (newDistance >= distances[indexNew])
                            {
                                break;
                            }
                            queue.Remove(queueNodes[indexNew]);
                            goto case PathfindingNodeState.Untouched;
                        case PathfindingNodeState.Untouched:
                            distances[indexNew] = newDistance;
                            parents[indexNew] = curNode.pos;
                            nodeStates[indexNew] = PathfindingNodeState.Open;
                            var newNode = new Pathfinding3DQueueNode(new Vector3Int(x, y, z));
                            queueNodes[indexNew] = newNode;
                            queue.Enqueue(newNode, newDistance);
                            break;
                    }
                }
            }
        }

        if (nodeStates[toIndex] == PathfindingNodeState.Closed)
        {
            var path = new List<Vector3>(w * 4);
            var curNode = toRounded;
            while (curNode != fromRounded)
            {
                int curIndex = Index(curNode);
                Assert.IsTrue(path.Count < w * h);
                Assert.AreEqual(PathfindingNodeState.Closed, nodeStates[curIndex]);
                path.Add(curNode);
                var nextNode = parents[curIndex];
                Assert.AreNotEqual(nextNode, curNode);
                curNode = nextNode;
            }

            path.Reverse();
            return new PathfindingResult(true, path);
        }

        return new PathfindingResult();
    }

    private float Heuristic(Vector3 a, Vector3 b)
    {
        return Vector3.Distance(a, b);
    }

    private int Index(Vector3Int vec)
    {
        return vec.x + vec.z * w;
    }

    private bool InBounds(int index)
    {
        return index >= 0 && index < w * h;
    }

    private static Vector3Int RoundVector(Vector3 vec)
    {
        return new Vector3Int(Mathf.RoundToInt(vec.x), Mathf.RoundToInt(vec.y), Mathf.RoundToInt(vec.z));
    }
}
