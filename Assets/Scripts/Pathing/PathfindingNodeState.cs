﻿public enum PathfindingNodeState : byte
{
    Untouched = 0,
    Open,
    Closed
}
