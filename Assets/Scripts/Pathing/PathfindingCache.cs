﻿using System;
using UnityEngine;
using System.Linq;
using UnityEngine.Assertions;
using System.Collections.Generic;
using Random = UnityEngine.Random;

[Serializable]
public class PathfindingCache
{
    public int W { get; }

    public int H { get; }

    public IReadOnlyList<float> Hmap { get; }

    public IReadOnlyList<float> Costs { get; }

    public PathfindingCache(int w, int h, float[] hmap, float[] costs)
    {
        Ensure.NotNull(hmap);
        Ensure.NotNull(costs);
        this.W = w;
        this.H = h;
        this.Hmap = hmap;
        this.Costs = costs;
        Assert.AreEqual(w * h, hmap.Length);
        Assert.AreEqual(w * h, costs.Length);
    }

    public static PathfindingCache FromHeightmap(float[] hmap, int w, int h)
    {
        Ensure.NotNull(hmap);
        Assert.AreEqual(w * h, hmap.Length);
        var costs = Enumerable.Repeat(-1.0f, w * h).ToArray();

        for (int x = 1; x < w - 1; x++)
        {
            for (int y = 1; y < h - 1; y++)
            {
                var ul = hmap[x + y * w];
                var ur = hmap[x + 1 + y * w];
                var ll = hmap[x + (y + 1) * w];
                var lr = hmap[x + 1 + (y + 1) * w];
                float[] vals = { ul, ur, ll, lr };
                if (vals.Max() > vals.Min() + 1)
                {
                    costs[x + y * w] = vals.Max() - vals.Min();
                }
                else
                {
                    costs[x + y * w] = 1.0f;
                }
            }
        }

        GenerateRooms(costs, w, h);
        return new PathfindingCache(w, h, hmap, costs);
    }

    private static void GenerateRooms(float[] costs, int w, int h)
    {
        var rooms = new List<RectInt>();
        for (int i = 0; i < 10; i++)
        {
            int xMin = Random.Range(1, w - 10);
            int yMin = Random.Range(1, h - 10);
            int xMax = Random.Range(10, w - 1);
            int yMax = Random.Range(10, h - 1);
            if (xMin > xMax)
            {
                (xMin, xMax) = (xMax, xMin);
            }

            if (yMin > yMax)
            {
                (yMin, yMax) = (yMax, yMin);
            }

            var newRoom = new RectInt(xMin, yMin, xMax - xMin, yMax - yMin);
            rooms.Add(newRoom);
        }

        foreach (var room in rooms)
        {
            for (int y = room.yMin; y < room.yMax; y++)
            {
                int indexLeft = Index.Of(room.xMin, y, w);
                costs[indexLeft] = -1.0f;
                int indexRight = Index.Of(room.xMax, y, w);
                costs[indexRight] = -1.0f;
            }

            for (int x = room.xMin; x < room.xMax; x++)
            {
                int indexUp = Index.Of(x, room.yMin, w);
                costs[indexUp] = -1.0f;
                int indexDown = Index.Of(x, room.yMax, w);
                costs[indexDown] = -1.0f;
            }
        }

        foreach (var room in rooms)
        {
            for (int x = room.xMin + 1; x < room.xMax - 1; x++)
            {
                for (int y = room.yMin + 1; y < room.yMax - 1; y++)
                {
                    costs[Index.Of(x, y, w)] = 1.0f;
                }
            }
        }

        foreach (var room in rooms)
        {
            for (int i = 0; i < 4; i++)
            {
                var pt = new Vector2Int(Random.Range(room.xMin, room.xMax), Random.Range(room.yMin, room.yMax));
                switch (Random.Range(0, 3))
                {
                    case 0:
                        pt.x = room.xMin;
                        break;
                    case 1:
                        pt.y = room.yMin;
                        break;
                    case 2:
                        pt.x = room.xMax;
                        break;
                    case 3:
                        pt.y = room.yMax;
                        break;
                }

                costs[Index.Of(pt.x, pt.y, w)] = 1.0f;
            }
        }
    }
}
