﻿using System.Linq;
using Assets.Scripts.CritterAI.Actions;
using UnityEngine;
using Utils.Extensions;

[RequireComponent(typeof(LineRenderer))]
public class ManualQueuePreview : MonoBehaviour
{
    private LineRenderer lineRenderer;

    private void Awake()
    {
        lineRenderer = this.GetComponentNotNull<LineRenderer>();
    }

    // TODO: Make it observe automatically
    public void Preview(ActionQueue actionQueue)
    {
        var points = actionQueue.OfType<MoveAction>().SelectMany(x => x.Path).ToArray();
        lineRenderer.positionCount = points.Length;
        lineRenderer.SetPositions(points);
    }
}
