﻿using Priority_Queue;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

namespace Assets.Pathing
{
    public class DistanceField
    {
        private readonly PathfindingCache pathfindingCache;
        private readonly int w;
        private readonly int h;

        public Vector2Int Center { get; }
        public float[] Distances { get; }

        public DistanceField(PathfindingCache pathfindingCache, Vector2Int center)
        {
            Ensure.NotNull(pathfindingCache);
            this.pathfindingCache = pathfindingCache;
            this.w = pathfindingCache.W;
            this.h = pathfindingCache.H;
            this.Center = center;
            this.Distances = Calculate();
        }

        private float[] Calculate()
        {
            var nodeStates = new PathfindingNodeState[w * h];
            var queueNodes = new Pathfinding2DQueueNode[w * h];
            var distances = Enumerable.Repeat(-1f, w * h).ToArray();
            var parents = new Vector2Int[w * h];

            var queue = new FastPriorityQueue<Pathfinding2DQueueNode>(maxNodes: w * h);
            var fromNode = new Pathfinding2DQueueNode(Center);
            int fromIndex = Index(Center);
            
            Assert.IsTrue(InBounds(Index(Center)));

            if (pathfindingCache.Costs[fromIndex] < 0.0f)
            {
                return distances;
            }

            queueNodes[Center.x + Center.y * w] = fromNode;
            nodeStates[fromIndex] = PathfindingNodeState.Open;
            queue.Enqueue(fromNode, 0.0f);

            while (queue.Count > 0)
            {
                var curNode = queue.Dequeue();
                int index = Index(curNode.pos);
                Assert.IsTrue(InBounds(index));
                Assert.AreEqual(PathfindingNodeState.Open, nodeStates[index]);
                nodeStates[index] = PathfindingNodeState.Closed;
                
                float curDistance = distances[index];

                int minX = Math.Max(0, curNode.pos.x - 1);
                int maxX = Math.Min(w - 1, curNode.pos.x + 1);
                int minZ = Math.Max(0, curNode.pos.y - 1);
                int maxZ = Math.Min(h - 1, curNode.pos.y + 1);
                for (int x = minX; x <= maxX; x++)
                {
                    for (int z = minZ; z <= maxZ; z++)
                    {
                        int indexNew = x + z * w;
                        Assert.IsTrue(InBounds(indexNew));
                        float moveCostBase = pathfindingCache.Costs[indexNew];
                        if (moveCostBase < 0.0f)
                        {
                            nodeStates[indexNew] = PathfindingNodeState.Closed;
                            continue;
                        }

                        int y = Mathf.RoundToInt(pathfindingCache.Hmap[indexNew]);
                        var newPoint = new Vector3(x, y, z);

                        float moveCostMult = (x == curNode.pos.x || z == curNode.pos.y) ? 1.0f : 1.41f;
                        float moveCost = moveCostBase * moveCostMult;
                        var newDistance = curDistance + moveCost;
                        float newPriority = newDistance;
                        switch (nodeStates[indexNew])
                        {
                            case PathfindingNodeState.Closed:
                                continue;
                            case PathfindingNodeState.Open:
                                if (newDistance >= distances[indexNew])
                                {
                                    break;
                                }
                                queue.Remove(queueNodes[indexNew]);
                                goto case PathfindingNodeState.Untouched;
                            case PathfindingNodeState.Untouched:
                                distances[indexNew] = newDistance;
                                parents[indexNew] = curNode.pos;
                                nodeStates[indexNew] = PathfindingNodeState.Open;
                                var newNode = new Pathfinding2DQueueNode(new Vector2Int(x, y));
                                queueNodes[indexNew] = newNode;
                                queue.Enqueue(newNode, newDistance);
                                break;
                        }
                    }
                }
            }

            return distances;
        }

        private int Index(Vector2Int vec)
        {
            return vec.x + vec.y * w;
        }

        private bool InBounds(int index)
        {
            return index >= 0 && index < w * h;
        }
    }
}