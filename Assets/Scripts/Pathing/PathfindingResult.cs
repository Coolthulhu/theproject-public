﻿using UnityEngine;
using System.Collections.Generic;

public class PathfindingResult
{
    public PathfindingResult()
    {
        Success = false;
        Path = new List<Vector3>();
    }

    public PathfindingResult(bool success, IReadOnlyList<Vector3> path)
    {
        Success = success;
        Path = path;
    }

    public bool Success { get; }
    public IReadOnlyList<Vector3> Path { get; }
}
