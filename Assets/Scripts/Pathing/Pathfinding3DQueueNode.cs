﻿using UnityEngine;
using Priority_Queue;

public class Pathfinding3DQueueNode : FastPriorityQueueNode
{
    public readonly Vector3Int pos;

    public Pathfinding3DQueueNode(Vector3Int pos)
    {
        this.pos = pos;
    }
}

public class Pathfinding2DQueueNode : FastPriorityQueueNode
{
    public readonly Vector2Int pos;

    public Pathfinding2DQueueNode(Vector2Int pos)
    {
        this.pos = pos;
    }
}
