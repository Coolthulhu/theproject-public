﻿using UnityEngine;

public class PostProcessOutline : MonoBehaviour
{
    private Camera attachedCamera;
    private GameObject cameraContainer;
    private Camera tempCam;
    private Material postMat;
    private Material simpleMat;

    [SerializeField]
    private Shader postOutline = null;
    [SerializeField]
    private Shader drawSimple = null;
    [SerializeField]
    private bool withReplacement = true;


    private void Awake()
    {
        attachedCamera = GetComponent<Camera>();
        cameraContainer = new GameObject();
        cameraContainer.transform.SetParent(transform, false);
        tempCam = cameraContainer.AddComponent<Camera>();
        tempCam.enabled = false;
        postMat = new Material(postOutline);
        simpleMat = new Material(drawSimple);
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        tempCam.CopyFrom(attachedCamera);
        tempCam.clearFlags = CameraClearFlags.Color;
        tempCam.backgroundColor = new Color(0, 0, 0, 0);
        tempCam.cullingMask = 1 << LayerMask.NameToLayer("Outline");
        
        var TempRT = new RenderTexture(source.width, source.height, 0, RenderTextureFormat.ARGB32);
        
        TempRT.Create();

        tempCam.targetTexture = TempRT;
        if (withReplacement)
        {
            tempCam.SetReplacementShader(drawSimple, "");
        }
        else
        {
            tempCam.ResetReplacementShader();
        }

        tempCam.Render();

        postMat.SetTexture("_OutlineTex", TempRT);
        postMat.SetTexture("_SceneTex", source);
        
        Graphics.Blit(source, destination);
        Graphics.Blit(source, destination, postMat);
        
        TempRT.Release();
    }

}