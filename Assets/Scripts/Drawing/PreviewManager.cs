﻿using System.Collections.Generic;
using Assets.Utils;
using UnityEngine;
using UnityEngine.Assertions;

namespace Assets.Drawing
{
    public class PreviewManager : MonoBehaviour
    {
        [SerializeField]
        private GameObject mouseoverChildPrefab = null;
        
        [SerializeField]
        private GameObject selectionMarkerPrefab = null;

        [SerializeField]
        private UIController uiController = null;

        private Dictionary<Selectable, PreviewedObject> previewedObjects = new Dictionary<Selectable, PreviewedObject>();

        private GameObject lastMouseoverObject = null;

        private PreviewedObject lastMouseoverPreviewObject = null;

        public void Clear()
        {
            UnsetSelection();
            RemoveLastMouseover();
        }

        public void HandleMouseover(GameObject mousedOver)
        {
            if (mousedOver == lastMouseoverObject)
            {
                return;
            }

            var oldSelectable = lastMouseoverObject?.GetComponent<Selectable>();
            bool needsReselect = oldSelectable != null && previewedObjects.ContainsKey(oldSelectable);
            var oldMouseoverPreview = lastMouseoverPreviewObject;
            RemoveLastMouseover();
            SetMouseover(mousedOver);

            if (needsReselect)
            {
                var oldSelectionMarker = previewedObjects[oldSelectable];
                RemovePreview(oldSelectionMarker);

                bool removed = previewedObjects.Remove(oldSelectable);
                Assert.IsTrue(removed);
                SelectOne(oldSelectable);
            }
        }

        public void Preview(IEnumerable<PreparedUIOrder> orders)
        {
            foreach (var preparedUiOrder in orders)
            {
                var selectable = preparedUiOrder.SelectedActor.Selectable;
                var actionQueue = preparedUiOrder.ActionQueue;

                Assert.IsTrue(previewedObjects.ContainsKey(selectable));
                var previewedObject = previewedObjects[selectable];
                previewedObject.ManualQueuePreview.Preview(actionQueue);
            }
        }

        public void SetSelection(Selection selection)
        {
            Clear();

            foreach (var selected in selection)
            {
                SelectOne(selected.Selectable);
            }
        }

        private static void AttachTrackers(GameObject parentOfTrackers, Selectable trackedObject)
        {
            var trackers = parentOfTrackers.GetComponents<ITracksObject>();
            foreach (var tracker in trackers)
            {
                tracker.Tracked = trackedObject.gameObject;
            }
        }

        private static void UndoEffects(Selectable selectable)
        {
            foreach (var undoable in selectable.GetComponents<IUndoable>())
            {
                var component = undoable as Component;
                if (component)
                {
                    undoable.Undo();
                    Destroy(component);
                }
            }
        }

        private void UnsetSelection()
        {
            foreach (var previewedObjectPair in previewedObjects)
            {
                RemovePreview(previewedObjectPair.Value);
            }

            previewedObjects.Clear();
        }

        private static void RemovePreview(PreviewedObject previewedObject)
        {
            previewedObject.Undo();
            Destroy(previewedObject.Marker);
        }

        private void Awake()
        {
            Assert.IsNotNull(uiController);
            Assert.IsNotNull(mouseoverChildPrefab);
            Assert.IsNotNull(selectionMarkerPrefab);
        }

        private void RemoveLastMouseover()
        {
            if (lastMouseoverObject == null)
            {
                return;
            }

            var previewObject = lastMouseoverPreviewObject;
            lastMouseoverPreviewObject = null;
            if (previewObject == null)
            {
                return;
            }
            
            RemovePreview(previewObject);
        }

        private void SelectOne(Selectable toSelect)
        {
            Assert.IsFalse(previewedObjects.ContainsKey(toSelect));
            var marker = GameObject.Instantiate(selectionMarkerPrefab, toSelect.transform);
            var queuePreview = marker.GetComponent<ManualQueuePreview>();
            Assert.IsNotNull(toSelect);
            Assert.IsNotNull(marker);
            Assert.IsNotNull(queuePreview);
            AttachTrackers(marker, toSelect);
            var previewedObject = new PreviewedObject(toSelect, marker, queuePreview);
            previewedObject.Apply();
            previewedObjects[toSelect] = previewedObject;
        }

        private void SetMouseover(GameObject mousedOver)
        {
            lastMouseoverObject = mousedOver;

            if (mousedOver == null)
            {
                return;
            }

            var selectable = mousedOver.GetComponent<Selectable>();
            if (selectable == null)
            {
                return;
            }

            var newOutline = Instantiate(mouseoverChildPrefab, uiController.Canvas.transform, false);
            var pathPreview = newOutline.GetComponentInChildren<ManualQueuePreview>();
            Assert.IsNotNull(newOutline);
            // Assert.IsNotNull(pathPreview);
            AttachTrackers(newOutline, selectable);
            var previewedObject = new PreviewedObject(selectable, newOutline, pathPreview);
            previewedObject.Apply();
            lastMouseoverPreviewObject = previewedObject;
        }

        private class PreviewedObject
        {
            public PreviewedObject(Selectable selectable, GameObject marker, ManualQueuePreview manualQueuePreview)
            {
                Selectable = selectable;
                Marker = marker;
                ManualQueuePreview = manualQueuePreview;
                Undoables = Marker.GetComponents<IUndoable>();
            }

            public ManualQueuePreview ManualQueuePreview { get; }
            public Selectable Selectable { get; }
            public GameObject Marker { get; }
            public IUndoable[] Undoables { get; }

            public void Apply()
            {
                foreach (var undoable in Undoables)
                {
                    undoable.Do();
                }
            }

            public void Undo()
            {
                foreach (var undoable in Undoables)
                {
                    undoable.Undo();
                }
            }
        }
    }
}