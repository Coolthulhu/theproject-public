﻿Shader "Custom/Post Outline"
{
    Properties
    {
        _OutlineTex( "Outline Texture",2D ) = "black"{}
        _SceneTex( "Scene Texture",2D ) = "black"{}
    }
        
    SubShader
    {
        Tags
        {
            "Queue" = "Transparent"
            "RenderType" = "Transparent"
        }

        CGINCLUDE

        struct v2f
        {
            float4 pos : POSITION;
            float2 uvs : TEXCOORD0;
        };

        half4 blur( sampler2D tex, float2 uv, float2 step ) : COLOR
        {
            int iter_count = 20;

            half4 color_in_radius = half4( 0, 0, 0, 0 );

            [unroll( iter_count )]
            for( int k = 0; k < iter_count; k += 1 )
            {
                float2 sub_texel = uv + step * ( k - iter_count / 2 );
                half4 tex_here = tex2D( tex, sub_texel );
                color_in_radius += half4( tex_here.rgb * tex_here.a, tex_here.a );
            }

            color_in_radius.a = max( color_in_radius.a, 0.00001 );
            color_in_radius.rgb = color_in_radius.rgb / color_in_radius.a;
            color_in_radius.a = color_in_radius.a * ( 1.0 / iter_count );
            return color_in_radius;
        }

        ENDCG

        Pass
        {
            CGPROGRAM
     
            sampler2D _OutlineTex;
 
            float2 _OutlineTex_TexelSize;
 
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            v2f vert (appdata_base v)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
                o.uvs = v.texcoord;
                 
                return o;
            }
             
             
            half4 frag(v2f i) : COLOR
            {
                return blur( _OutlineTex, i.uvs, float2( _OutlineTex_TexelSize.x, 0 ) );
            }
             
            ENDCG
 
        }
        //end pass    
         
        GrabPass
        {
        }
         
        Pass 
        {
            CGPROGRAM
 
            sampler2D _OutlineTex;
            sampler2D _GrabTexture;
 
            float2 _GrabTexture_TexelSize;
 
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
             
            v2f vert (appdata_base v) 
            {
                v2f o;
                o.pos = UnityObjectToClipPos( v.vertex );
                o.uvs = ComputeGrabScreenPos( o.pos );
                 
                return o;
            }
             
            half4 frag(v2f i) : COLOR
            {
                return blur( _GrabTexture, i.uvs, float2( 0, _GrabTexture_TexelSize.y ) );
            }
             
            ENDCG
 
        }
        //end pass  

        GrabPass
        {
            "_GrabTexture"
        }

        Pass
        {
            Blend One Zero

            CGPROGRAM

            sampler2D _OutlineTex;
            sampler2D _SceneTex;
            sampler2D _GrabTexture;

            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            v2f vert( appdata_base v )
            {
                v2f o;
                o.pos = UnityObjectToClipPos( v.vertex );
                o.uvs = ComputeGrabScreenPos( o.pos );
                return o;
            }

            half4 frag( v2f i ) : COLOR
            {
                // TODO: Why is y inverted here? Use a Unity built-in for that?
                float2 inverted_y_uvs = float2( i.uvs.x, 1 - i.uvs.y );
                // If something already exists underneath the fragment (in the original texture), discard the fragment.
                if( tex2D( _OutlineTex, inverted_y_uvs ).a > 0.1 )
                {
                    return tex2D( _SceneTex, inverted_y_uvs );
                }

                half4 grabbed_color = tex2D( _GrabTexture, i.uvs );
                grabbed_color.a = min( grabbed_color.a * grabbed_color.a * 16, 1.0f );
                return half4( grabbed_color.a * grabbed_color.rgb, grabbed_color.a ) + ( 1 - grabbed_color.a ) * tex2D( _SceneTex, inverted_y_uvs );
            }

            ENDCG
        }
        //end pass
    }
    //end subshader
}
//end shader