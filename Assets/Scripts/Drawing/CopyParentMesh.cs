﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.Assertions;

[RequireComponent(typeof(MeshFilter))]
public class CopyParentMesh : MonoBehaviour
{
    private void Awake()
    {
        var meshFilter = GetComponent<MeshFilter>();
        Assert.IsNotNull(meshFilter);
        var allParentMeshFilter = GetComponentsInParent<MeshFilter>();
        var parentMeshFilter = allParentMeshFilter.First(x => x != meshFilter);
        Assert.IsNotNull(parentMeshFilter);
        meshFilter.mesh = parentMeshFilter.mesh;
    }
}
