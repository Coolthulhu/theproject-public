﻿using System.Linq;
using Assets.Orders;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class AnimationTestScript : MonoBehaviour
{
    private Animator animator;
    private IPathProvider pathProvider;

    private int hasMoreHash = Animator.StringToHash("HasMore");

    private void Awake()
    {
        animator = GetComponent<Animator>();
        pathProvider = GetComponentInParent<IPathProvider>();
    }

    private void Update()
    {
        animator.SetBool(hasMoreHash, pathProvider.Path.Any());
    }
}
