﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Utils;
using UnityEngine;

public class LayerMover : MonoBehaviour, IUndoable
{
    [SerializeField, Layer]
    private int layer = 0;

    [SerializeField, Layer, Readonly]
    private int oldLayer = 0;

    private GameObject[] moved = Array.Empty<GameObject>();

    private bool isDone = false;

    private void MoveFromToLayer(GameObject[] toMove, int fromLayer, int toLayer)
    {
        foreach (var gameObject in toMove)
        {
            MoveOrWarn(gameObject, fromLayer, toLayer);
        }
    }

    private void MoveOrWarn(GameObject go, int fromLayer, int toLayer)
    {
        if (go.layer != fromLayer)
        {
            string fromLayerName = LayerMask.LayerToName(fromLayer);
            string toLayerName = LayerMask.LayerToName(toLayer);
            string gotLayerName = LayerMask.LayerToName(go.layer);
            Debug.LogWarningFormat(this, "Tried to move {0} from layer {1} to {2}, but it was in layer {3}.",
                go, fromLayerName, toLayerName, gotLayerName);
        }
        else
        {
            go.layer = toLayer;
        }
    }

    public void Do()
    {
        if (isDone)
        {
            Debug.LogWarningFormat(this, "Do twice!");
        }

        var selectable = GetComponentInParent<Selectable>();
        var toMove = selectable.InitialRenderers.Select(x => x.gameObject).ToArray();
        oldLayer = selectable.gameObject.layer;
        if (oldLayer == layer)
        {
            Debug.LogWarningFormat(this, "Old layer and new layer are both {0}!", LayerMask.LayerToName(layer));
            return;
        }

        MoveFromToLayer(toMove, oldLayer, layer);
        moved = toMove;

        isDone = true;
    }

    public void Undo()
    {
        if (!isDone)
        {
            Debug.LogWarningFormat(this, "Undo without Do!");
        }

        isDone = false;
        MoveFromToLayer(moved, layer, oldLayer);
    }
}
