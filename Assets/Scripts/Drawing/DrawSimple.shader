﻿Shader "Custom/DrawSimple"
{
    Properties {
        _Color( "Color", Color ) = ( 1, 1, 1, 1 )
    }

    SubShader
    {
        Tags { "RenderType" = "Opaque" }
        ZWrite Off
        ZTest Always
        Lighting Off
        Color[_Color]
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            half4 _Color;

            struct v2f
            {
                float4 pos : POSITION;
            };

            v2f vert( appdata_base v )
            {
                v2f o;
                o.pos = mul( UNITY_MATRIX_VP, mul( unity_ObjectToWorld, v.vertex ) );
                return o;
            }

            half4 frag() : COLOR
            {
                return _Color;
                return half4( 1, 0, 0, 1 );
            }

            ENDCG
        }
    }
}