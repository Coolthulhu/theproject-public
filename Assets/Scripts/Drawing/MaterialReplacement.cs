﻿using UnityEngine;
using System.Collections.Generic;
using Assets.Utils;
using UnityEngine.Assertions;

public class MaterialReplacement : MonoBehaviour, IUndoable
{
    [SerializeField]
    private Material[] replacementMaterials = null;
    
    private Dictionary<Renderer, Material[]> oldMaterials = new Dictionary<Renderer, Material[]>();

    public void Do()
    {
        var parent = gameObject.transform.parent;
        var renderersReplaced = parent.GetComponent<Selectable>().InitialRenderers;
        Assert.IsTrue(renderersReplaced.Count > 0);
        foreach (var renderer in renderersReplaced)
        {
            if (replacementMaterials != null)
            {
                oldMaterials[renderer] = renderer.materials;
                renderer.materials = replacementMaterials;
            }
        }
    }

    public void Undo()
    {
        foreach (var pair in oldMaterials)
        {
            if (pair.Key != null)
            {
                pair.Key.materials = pair.Value;
            }
        }

        oldMaterials.Clear();
    }
}
