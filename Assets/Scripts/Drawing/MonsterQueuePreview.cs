﻿using Assets.CritterAI;
using Assets.Scripts.CritterAI.Actions;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using Utils.Extensions;

[RequireComponent(typeof(LineRenderer))]
public class MonsterQueuePreview : MonoBehaviour
{
    private LineRenderer lineRenderer;
    private Monster monster;

    private void Awake()
    {
        lineRenderer = this.GetComponentNotNull<LineRenderer>();
        monster = GetComponentInParent<Monster>();
        if (!monster)
        {
            GameObject.Destroy(this);
        }
    }

    private void Update()
    {
        var actionQueue = monster.ActionQueue;
        if (actionQueue == null)
        {
            return;
        }

        var points = actionQueue.OfType<MoveAction>().SelectMany(x => x.Path).ToArray();
        lineRenderer.positionCount = points.Length;
        lineRenderer.SetPositions(points);
    }
}
