﻿using System;
using Assets.Actions;
using Assets.Scripts.AI.Decisions;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.CritterAI;
using Assets.Scripts.CritterAI.Actions;
using Priority_Queue;
using UnityEngine.Assertions;

namespace Assets.CritterAI
{
    public class AIQueueNode<T> : FastPriorityQueueNode
    {
        public AIQueueNode(T node, AIQueueNode<T> parent, float cost, IAIAction<T> aiAction)
        {
            Node = node;
            Parent = parent;
            Cost = cost;
            AIAction = aiAction;
        }

        public T Node { get; }

        public AIQueueNode<T> Parent { get; set; }
        public float Cost { get; set; }
        public IAIAction<T> AIAction { get; }
    }
}