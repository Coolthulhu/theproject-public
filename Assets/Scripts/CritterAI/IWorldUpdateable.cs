﻿namespace Assets.CritterAI
{
    public interface IWorldUpdateable
    {
        void WorldUpdate();
    }
}