﻿using Assets.Actions;
using Assets.Scripts.AI.Decisions;
using Assets.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.CritterAI.Actions;
using UnityEngine;
using Utils.Extensions;
using Assets.Scripts.CritterAI;
using Priority_Queue;
using UnityEngine.Assertions;

namespace Assets.CritterAI
{
    public class PlanResult
    {
        public PlanResult(bool success, IReadOnlyList<IActionInstance> plan)
        {
            Success = success;
            Plan = plan ?? Array.Empty<IActionInstance>();
        }

        public bool Success { get; }
        public IReadOnlyList<IActionInstance> Plan { get; }
    }

    [Serializable]
    public class ColonyBrainImpl
    {
        [SerializeField]
        private List<Monster> actors = new List<Monster>();

        private Dictionary<Pickup, int> availableAmount = new Dictionary<Pickup, int>();

        [SerializeField]
        private List<Dropoff> dropoffs = new List<Dropoff>();

        public void Act()
        {
            // TODO: Actor announces when idle
            var freeActors = actors.Where(x => x.IsIdle()).ToArray();
            if (freeActors.Length == 0)
            {
                return;
            }
            
            foreach (var actor in freeActors)
            {
                OrderMonster(actor);
            }
        }

        public void Register(Monster actor)
        {
            actors.Add(actor);
            actor.OrderApplying += ActorOnOrderApplying;
        }

        public void Register(Pickup pickup)
        {
            availableAmount[pickup] = pickup.Amount;
        }

        public void Register(Dropoff dropoff)
        {
            dropoffs.Add(dropoff);
        }

        public void Remove(Pickup pickup)
        {
            availableAmount.Remove(pickup);
        }

        public void Remove(Monster monster)
        {
            monster.OrderApplying -= ActorOnOrderApplying;
            actors.Remove(monster);
        }

        public void Remove(Dropoff dropoff)
        {
            dropoffs.Remove(dropoff);
        }

        private void ActorOnOrderApplying(IOrderable actor, ActionQueue actionQueue)
        {
            var monster = actor.GameObject.GetComponentNotNull<Monster>();
            var oldQueue = monster.ActionQueue;
            RemoveQueue(oldQueue);

            var newPickups = ExtractPickups(actionQueue);
            foreach (var pickup in newPickups)
            {
                availableAmount[pickup]--;
                if (availableAmount[pickup] <= 0)
                {
                    // TODO: Mega slow
                }
            }
        }

        private IEnumerable<Pickup> ExtractPickups(ActionQueue queue)
        {
            var pickupActions = queue.OfType<PickupAction>();
            return pickupActions.Select(x => x.Target);
        }

        public void OrderMonster(Monster monster)
        {
            var worldState = GetCurrentWorldState();
            // TODO: Remove
            worldState.currentMonster = monster;

            var actorState = MakeState(monster);
            var plan = Plan(monster.GoapActor, actorState, worldState);
            if (plan.Success)
            {
                monster.ActionQueue = new ActionQueue(monster, plan.Plan);
            }
        }

        public PlanResult Plan<NodeType, ActorType>(ActorType actor, NodeType actorState, WorldState worldState) where ActorType : IGOAPActor<NodeType>
        {
            Assert.IsTrue(actor.Goals.Count > 0);
            
            var goals = actor.Goals;

            var startNode = new AIQueueNode<NodeType>(actorState, null, 0f, null);
            var queue = new FastPriorityQueue<AIQueueNode<NodeType>>(1024 * 8);
            var open = new Dictionary<NodeType, AIQueueNode<NodeType>>();
            var closed = new HashSet<NodeType>();

            queue.Enqueue(startNode, 0.0f);

            AIQueueNode<NodeType> end = null;
            while (queue.Count > 0)
            {
                if (closed.Count > 1024 * 4)
                {
                    Debug.LogError("Planning too hueg");
                    return new PlanResult(false, null);
                }

                var parent = queue.Dequeue();
                if (goals.Any(goal => goal.SatisfiedBy(parent.Node, worldState)))
                {
                    // Found goal node
                    end = parent;
                    break;
                }

                closed.Add(parent.Node);
                open.Remove(parent.Node);

                var viableActions = actor.Actions
                    .Where(action => action.PreconditionsMet(parent.Node, actor, worldState)).ToArray();
                
                var edges = viableActions.SelectMany(action => action.Expand(parent.Node, actor, worldState)).ToArray();

                foreach (var edge in edges)
                {
                    var childNode = edge.Node;
                    if (closed.Contains(childNode))
                    {
                        continue;
                    }

                    var child = new AIQueueNode<NodeType>(edge.Node, parent, parent.Cost + edge.Cost, edge.Action);
                    var childHeuristic = goals.Min(goal => goal.Heuristic(child.Node, worldState));
                    if (open.ContainsKey(childNode))
                    {
                        child.Parent = parent;
                        queue.UpdatePriority(open[childNode], child.Cost + childHeuristic);
                    }
                    else
                    {
                        child.Parent = parent;
                        queue.Enqueue(child, child.Cost + childHeuristic);
                        open[childNode] = child;
                    }
                }
            }

            if (end == null)
            {
                return new PlanResult(false, null);
            }

            var actions = new List<IActionInstance>();
            var last = end;
            var path = new List<AIQueueNode<NodeType>>();
            // Skip first node since it has no action
            while (last.Parent != null)
            {
                path.Add(last);
                last = last.Parent;
            }

            path.Reverse();
            foreach (var queueNode in path)
            {
                Assert.IsNotNull(queueNode.Parent);
                var resultActions =
                    queueNode.AIAction.ActionsFromTo(queueNode.Parent.Node, queueNode.Node, actor, worldState);
                if (resultActions.Success)
                {
                    actions.AddRange(resultActions.Actions);
                }
                else
                {
                    return new PlanResult(false, actions);
                }
            }

            return new PlanResult(true, actions);
        }

        public WorldState GetCurrentWorldState()
        {
            var worldState = new WorldState();
            worldState.dropoffsByPosition = dropoffs.ToDictionary(x => x.transform.position, x => x);
            worldState.pickupsByPosition = availableAmount.ToDictionary(x => x.Key.transform.position, x => x.Key);
            worldState.endpoints = dropoffs.Select(x => x.transform.position)
                .Concat(availableAmount.Select(x => x.Key.transform.position))
                .ToList();
            return worldState;
        }

        private MonsterAINode MakeState(Monster actor)
        {
            return new MonsterAINode(position: actor.GameObject.transform.position, hasPickup: actor.GetComponent<InventoryComponent>().Inventory.Count > 0);
        }

        private void RemoveQueue(ActionQueue queue)
        {
            if (queue != null)
            {
                var oldPickups = ExtractPickups(queue);

                foreach (var pickup in oldPickups)
                {
                    availableAmount[pickup]++;
                }
            }
        }

        private MoveAction SendTo(Monster actor, Vector3 start, Vector3 end, float rangeSquared)
        {
            var pathfindingResult = actor.GoapActor.Pathfind(start, end);
            var path = pathfindingResult.Path.ToList();

            if (rangeSquared > 0.0f)
            {
                int firstOverDistance;
                for (firstOverDistance = path.Count - 1; firstOverDistance >= 0; firstOverDistance--)
                {
                    if (VectorMath.Distance2DSquared(path[firstOverDistance], end) > rangeSquared)
                    {
                        break;
                    }
                }

                path.RemoveRange(firstOverDistance + 1, path.Count - firstOverDistance - 1);
            }

            if (path.Count == 0)
            {
                return null;
            }

            return new MoveAction(actor, path);
        }
    }
}