﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.AI.Decisions;
using Assets.Scripts.CritterAI.Actions;
using UnityEngine;

namespace Assets.CritterAI
{
    public interface IGOAPActor<T>
    {
        IReadOnlyList<IAIAction<T>> Actions { get; }

        IReadOnlyList<IAIGoal<T>> Goals { get; }

        PathfindingSettings PathfindingSettings { get; }

        PathfindingResult Pathfind(Vector3 from, Vector3 to);
    }

    [Serializable]
    public class MonsterGOAPActor : IGOAPActor<MonsterAINode>, ISerializationCallbackReceiver
    {
        [SerializeField]
        private MonsterActionContainer[] serializableActions = null;

        [SerializeField]
        private MonsterAIGoal[] serializableGoals = null;

        [SerializeField]
        private PathfindingSettings serializablePathfindingSettings = null;

        public IReadOnlyList<IAIAction<MonsterAINode>> Actions { get; private set; }

        public IReadOnlyList<IAIGoal<MonsterAINode>> Goals { get; private set; }

        public PathfindingSettings PathfindingSettings { get; private set; }

        public void OnAfterDeserialize()
        {
            Actions = serializableActions?.Select(x => x.contained).Cast<IAIAction<MonsterAINode>>().ToList();
            Goals = serializableGoals?.Cast<IAIGoal<MonsterAINode>>().ToList();
            PathfindingSettings = serializablePathfindingSettings;
        }

        public void OnBeforeSerialize()
        {
        }

        public PathfindingResult Pathfind(Vector3 from, Vector3 to)
        {
            return PathfindingSettings.TerrainManager.Pathfind(from, to);
        }
    }
}