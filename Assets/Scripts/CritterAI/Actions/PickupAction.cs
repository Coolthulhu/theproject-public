﻿using Assets.Actions;
using Assets.CritterAI;
using UnityEngine;

namespace Assets.Scripts.CritterAI.Actions
{
    public class PickupAction : IActionInstance
    {
        private int work = 0;

        public int MaxWork = 100;
        private Monster picker;

        public float Progress => (float)work / MaxWork;
        
        public string Name => "Pickup";

        public string Animation => "Pickup";

        public bool DisplayProgress => true;

        public Pickup Target { get; }

        public PickupAction(Monster picker, Pickup target)
        {
            this.picker = picker;
            this.Target = target;
        }

        public void Act()
        {
            if (!Target)
            {
                Debug.LogWarning("Null pickup", picker);
                work = MaxWork;
                return;
            }

            work++;
            if (work < MaxWork)
            {
                return;
            }

            var pickablesManager = GameObject.FindObjectOfType<PickablesManager>();
            pickablesManager.PickupSpecific(picker.GetComponent<IItemPicker>(), Target);
        }
    }
}