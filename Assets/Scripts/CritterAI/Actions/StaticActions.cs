﻿using Assets.Actions;

namespace Assets.Scripts.CritterAI.Actions
{
    public class StaticActions
    {
        private static readonly StaticActions instance = new StaticActions();
        private readonly ActionDefinition pickup;

        private StaticActions()
        {
            pickup = new ActionDefinition("pickup", 100f);
        }

        public static ActionDefinition Pickup
        {
            get => instance.pickup;
        }
    }
}