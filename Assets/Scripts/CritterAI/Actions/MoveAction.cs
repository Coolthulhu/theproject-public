﻿using System.Collections.Generic;
using Assets.Actions;
using Assets.CritterAI;
using UnityEngine;

namespace Assets.Scripts.CritterAI.Actions
{
    public class MoveAction : IActionInstance
    {
        private Monster actor;
        private int originalPathCount;

        public float Progress => 1.0f - ((float)Path.Count / originalPathCount);

        public string Name => "Move";

        public string Animation => "Walk";

        public bool DisplayProgress => true;

        // TODO: Hacky
        public List<Vector3> Path { get; private set; }

        public MoveAction(Monster actor, List<Vector3> path)
        {
            this.actor = actor;
            Path = path;
            originalPathCount = Mathf.Max(path.Count, 1);
        }

        public void Act()
        {
            actor.FollowPath(Path);
        }
    }
}