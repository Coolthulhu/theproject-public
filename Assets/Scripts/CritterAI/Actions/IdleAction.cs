﻿using Assets.Actions;

namespace Assets.Scripts.CritterAI.Actions
{
    public class IdleAction : IActionInstance
    {
        public float Progress => initialWork > 0 ? (float)work / initialWork : 1f;

        public string Name => "Idle";

        public string Animation => "Idle";

        public bool DisplayProgress => false;

        public int work = 0;

        public readonly int initialWork = 0;

        public IdleAction() : this(10)
        {
        }

        public IdleAction(int work)
        {
            this.work = work;
            initialWork = work;
        }

        public void Act()
        {
            if (work > 0)
            {
                work--;
            }
        }
    }
}