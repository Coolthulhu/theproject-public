﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Assets.Actions;
using Assets.CritterAI;
using Assets.Scripts.AI.Decisions;
using Assets.Utils;
using UnityEngine;
using UnityEngine.Assertions;

namespace Assets.Scripts.CritterAI.Actions
{
    public struct ActionExecutionResult
    {
        public ActionExecutionResult(bool success, IEnumerable<IActionInstance> actions)
        {
            Success = success;
            Actions = actions;
        }

        public bool Success { get; }
        public IEnumerable<IActionInstance> Actions { get; }
    }

    public interface IAIAction<T>
    {
        ActionExecutionResult ActionsFromTo(T from, T to, IGOAPActor<T> actor, WorldState worldState);
        IEnumerable<AIEdge<T>> Expand(T node, IGOAPActor<T> actor, WorldState worldState);
        bool PreconditionsMet(T node, IGOAPActor<T> actor, WorldState worldState);
    }

    [Serializable]
    public class MonsterAIGenericAction : IAIAction<MonsterAINode>, ISerializationCallbackReceiver
    {
        [SerializeField]
        public int cost;

        [SerializeField]
        public List<StringBoolPair> preconditions;

        [SerializeField]
        public List<StringBoolPair> effects;
        
        [SerializeReference]
        public List<AtomicMonsterAction> actions;

        [NonSerialized]
        private Func<MonsterAINode, bool> preconditionsCheck;

        [NonSerialized]
        private Action<MonsterAINode> effectsApply;

        public ActionExecutionResult ActionsFromTo(MonsterAINode from, MonsterAINode to, IGOAPActor<MonsterAINode> actor, WorldState worldState)
        {
            // TODO: Remove horrible
            var horrible = (Monster) worldState.currentMonster;

            var resultActions = new List<IActionInstance>(actions.Count);

            // TODO: Rewrite as objects
            foreach (var atomicMonsterAction in actions)
            {
                if (atomicMonsterAction.GetType() == typeof(AIDropoffAction))
                {
                    resultActions.Add(new DropoffAction(horrible, worldState.dropoffsByPosition[to.Position]));
                }
                else if (atomicMonsterAction.GetType() == typeof(AIMoveToEndpointAction))
                {
                    var pathfindingResult = actor.Pathfind(@from.Position, to.Position);
                    if (pathfindingResult.Success)
                    {
                        resultActions.Add(new MoveAction(horrible, pathfindingResult.Path.ToList()));
                    }
                    else
                    {
                        return new ActionExecutionResult();
                    }
                } else if (atomicMonsterAction.GetType() == typeof(AIPickupAction))
                {
                    resultActions.Add(new PickupAction(horrible, worldState.pickupsByPosition[to.Position]));
                }
            }

            return new ActionExecutionResult(true, resultActions);
        }

        public IEnumerable<AIEdge<MonsterAINode>> Expand(MonsterAINode node, IGOAPActor<MonsterAINode> actor, WorldState worldState)
        {
            var newNode = node.Clone();

            effectsApply(newNode);
            
            // TODO: Remove hacks
            if (!actions.Any(x => x.GetType() == typeof(AIMoveToEndpointAction)))
            {
                var newEdge = new AIEdge<MonsterAINode>(newNode, this, cost);
                return new AIEdge<MonsterAINode>[] {newEdge};
            }
            else
            {
                var endpointMoveActions = actions.OfType<AIMoveToEndpointAction>().ToArray();
                Assert.IsTrue(endpointMoveActions.Count() == 1);
                var endpointAction = endpointMoveActions.First();

                bool isPickup = endpointAction.endpointType == AIMoveToEndpointAction.EndpointType.Pickups;
                
                newNode.AtPickup = isPickup;
                newNode.AtDropoff = !isPickup;

                var endpointsByType = isPickup
                    ? worldState.pickupsByPosition.Keys.ToArray()
                    : worldState.dropoffsByPosition.Keys.ToArray();
                var edges = new List<AIEdge<MonsterAINode>>(worldState.endpoints.Count);
                foreach (var endpoint in endpointsByType)
                {
                    var endpointNode = newNode.Clone();
                    endpointNode.Position = endpoint;
                    var newEdge = new AIEdge<MonsterAINode>(endpointNode, this, VectorMath.Distance2DSquared(node.Position, endpoint));
                    edges.Add(newEdge);
                }

                return edges;
            }
        }

        public bool PreconditionsMet(MonsterAINode node, IGOAPActor<MonsterAINode> actor, WorldState worldState)
        {
            return preconditionsCheck(node);
        }

        public void OnBeforeSerialize()
        {
        }

        public void OnAfterDeserialize()
        {
            // TODO: Get rid of the extra function here, since Apply needs to get setters anyway
            TypeUtils.GetTypedPropertyAccessors<MonsterAINode, bool>(out var getters, out var setters);

            preconditionsCheck = MakeCheckExpression(getters);
            effectsApply = MakeApplyExpression(setters);
        }

        private Action<MonsterAINode> MakeApplyExpression(Dictionary<string, Action<MonsterAINode, bool>> setters)
        {
            var node = Expression.Parameter(typeof(MonsterAINode), "node");
            
            var block = Expression.Block(effects.Select(eff =>
            {
                var setterInfo = typeof(MonsterAINode).GetProperty(eff.Key).GetSetMethod();
                var constValue = Expression.Constant(eff.Value);
                return Expression.Call(node, setterInfo, constValue);
            }));
            return Expression.Lambda<Action<MonsterAINode>>(block, node).Compile();
        }

        private Func<MonsterAINode, bool> MakeCheckExpression(Dictionary<string, Func<MonsterAINode, bool>> getters)
        {
            var node = Expression.Parameter(typeof(MonsterAINode), "node");
            var result = Expression.Parameter(typeof(int), "result");

            Expression expression = Expression.Constant(true);
            foreach (var stringBoolPair in preconditions)
            {
                var getter = getters[stringBoolPair.Key];
                var actual = Expression.Property(node, stringBoolPair.Key);
                var expected = Expression.Constant(stringBoolPair.Value, typeof(bool));
                var localExpression = Expression.Equal(actual, expected);
                expression = Expression.And(expression, localExpression);
            }

            return Expression.Lambda<Func<MonsterAINode, bool>>(expression, node).Compile();
        }
    }

    [Serializable]
    public struct StringBoolPair
    {
        [SerializeField]
        private string key;

        [SerializeField]
        private bool value;

        public string Key => key;

        public bool Value => value;

        public StringBoolPair(string k, bool v)
        {
            key = k;
            value = v;
        }

        public override bool Equals(object obj)
        {
            if (obj is StringBoolPair other)
            {
                return key == other.key && value == other.value;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return key.GetHashCode();
        }

        public static bool operator ==(StringBoolPair left, StringBoolPair right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(StringBoolPair left, StringBoolPair right)
        {
            return !(left == right);
        }
    }
}