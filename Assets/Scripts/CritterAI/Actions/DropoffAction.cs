﻿using Assets.Actions;
using Assets.CritterAI;

namespace Assets.Scripts.CritterAI.Actions
{
    public class DropoffAction : IActionInstance
    {
        private const int MaxWork = 10;
        private Monster actor;
        private Dropoff dropoff;

        private int work = 0;

        public DropoffAction(Monster actor, Dropoff dropoff)
        {
            this.actor = actor;
            this.dropoff = dropoff;
        }

        public float Progress => (float) work / MaxWork;

        public string Name => "Dropoff";

        public string Animation => "Dropoff";

        public bool DisplayProgress => true;

        public void Act()
        {
            work++;
            if (work < MaxWork)
            {
                return;
            }

            actor.GetComponent<InventoryComponent>().Inventory.Clear();
        }
    }
}