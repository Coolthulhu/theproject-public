﻿using System;
using UnityEngine;

namespace Assets.Scripts.CritterAI.Actions
{
    // TODO: Rewrite into proper interface
    public interface AtomicMonsterAction
    {
    }

    [Serializable]
    public class AIDropoffAction : AtomicMonsterAction
    {
    }

    [Serializable]
    public class AIPickupAction : AtomicMonsterAction
    {
    }

    [Serializable]
    public class AIMoveToEndpointAction : AtomicMonsterAction
    {
        public enum EndpointType
        {
            Pickups,
            Dropoffs
        }

        [SerializeField]
        public EndpointType endpointType;
    }
}