﻿using Assets.Scripts.AI.Decisions;

namespace Assets.Scripts.CritterAI.Actions
{
    public struct AIEdge<NodeType>
    {
        public NodeType Node { get; }
        public float Cost { get; }
        public IAIAction<NodeType> Action { get; }

        public AIEdge(NodeType node, IAIAction<NodeType> action, float cost)
        {
            Node = node;
            Action = action;
            Cost = cost;
        }
    }
}