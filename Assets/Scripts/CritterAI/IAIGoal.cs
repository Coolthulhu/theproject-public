﻿using System;
using System.Collections.Generic;
using Assets.Scripts.AI.Decisions;
using Assets.Scripts.CritterAI;
using UnityEngine;

namespace Assets.CritterAI
{
    public interface IAIGoal<T>
    {
        float Heuristic(T node, WorldState worldState);
        bool SatisfiedBy(T node, WorldState worldState);
    }

    [Serializable]
    public class MonsterAIGoal : IAIGoal<MonsterAINode>
    {
        [SerializeField]
        private MonsterAINode goal;

        public float Heuristic(MonsterAINode node, WorldState worldState)
        {
            return (node.DidDropoff ? 0.0f : 1.0f) +
                   (node.AtDropoff ? 0.0f : 0.4f) +
                   (node.HasPickup ? 0.0f : 0.2f) +
                   (node.AtPickup ? 0.0f : 0.1f);
        }

        public bool SatisfiedBy(MonsterAINode node, WorldState worldState)
        {
            return node.DidDropoff;
        }
    }
}