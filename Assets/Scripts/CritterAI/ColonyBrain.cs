﻿using Assets.Communication;
using UnityEngine;
using UnityEngine.Assertions;

namespace Assets.CritterAI
{
    public class ColonyBrain : MonoBehaviour, IWorldUpdateable, IMessageReceiver<CreatedMessage>, IMessageReceiver<DestroyedMessage>
    {
        [SerializeField]
        private TerrainManager terrainManager = null;

        private ColonyBrainImpl colonyBrainImpl = null;
        
        private void Awake()
        {
            Assert.IsNotNull(terrainManager);
            colonyBrainImpl = new ColonyBrainImpl();
        }

        public void Initialize(MessageTracker messageTracker)
        {
            messageTracker.RegisterReceiver<CreatedMessage>(this);
            messageTracker.RegisterReceiver<DestroyedMessage>(this);
        }

        public void WorldUpdate()
        {
            colonyBrainImpl.Act();
        }

        public void Receive(CreatedMessage message)
        {
            var gameObject = message.GameObject;

            var pickup = gameObject.GetComponent<Pickup>();
            if (pickup)
            {
                colonyBrainImpl.Register(pickup);
            }

            var monster = gameObject.GetComponent<Monster>();
            if (monster)
            {
                colonyBrainImpl.Register(monster);
            }

            var dropoff = gameObject.GetComponent<Dropoff>();
            if (dropoff)
            {
                colonyBrainImpl.Register(dropoff);
            }
        }

        public void Receive(DestroyedMessage message)
        {
            var gameObject = message.GameObject;

            var pickup = gameObject.GetComponent<Pickup>();
            if (pickup)
            {
                colonyBrainImpl.Remove(pickup);
            }

            var monster = gameObject.GetComponent<Monster>();
            if (monster)
            {
                colonyBrainImpl.Remove(monster);
            }

            var dropoff = gameObject.GetComponent<Dropoff>();
            if (dropoff)
            {
                colonyBrainImpl.Remove(dropoff);
            }
        }
    }
}