﻿using System;
using UnityEngine;

namespace Assets.CritterAI
{
    [Serializable]
    public class PathfindingSettings
    {
        [SerializeField]
        private TerrainManager terrainManager = null;

        public TerrainManager TerrainManager => terrainManager;
    }
}