﻿using System.Collections.Generic;
using System.Linq;
using Assets.Actions;
using Assets.Scripts.CritterAI.Actions;
using Assets.Utils;
using UnityEngine;
using UnityEngine.Assertions;
using Utils.Extensions;

namespace Assets.CritterAI
{
    [RequireComponent(typeof(InventoryComponent))]
    public class Monster : MonoBehaviour, IOrderable, IWorldUpdateable
    {
        [SerializeField]
        private float velocity = 1f;

        [SerializeField]
        private float pickupRangeSquared = 100f;

        private Animator animator;
        private InventoryComponent inventoryComponent;

        private IActionInstance actionInstance;

        private UIController uiController;
        private PickablesManager pickablesManager;
        private TerrainManager terrainManager;
        private ActionQueue actionQueue;

        public event OrderAppliedEventHandler OrderApplying;

        public MonsterGOAPActor GoapActor;

        public IActionInstance ActionInstance
        {
            get => actionInstance;
            set
            {
                actionInstance = value ?? new IdleAction();
                uiController.ShowActionProgress(gameObject, actionInstance);
                SetAnimation(actionInstance.Animation);
            }
        }

        public GameObject GameObject => gameObject;

        public ActionQueue ActionQueue
        {
            get => actionQueue;
            set
            {
                OrderApplying(this, value);
                actionQueue = value;
                ActionInstance = actionQueue.Peek();
            }
        }

        public float PickupRangeSquared { get => pickupRangeSquared; }

        public bool IsIdle()
        {
            return ActionInstance.Name == "Idle" && ActionInstance.Progress >= 1f;
        }

        private void Awake()
        {
            animator = GetComponentInChildren<Animator>();
            Assert.IsNotNull(animator);
            inventoryComponent = this.GetComponentNotNull<InventoryComponent>();

            uiController = FindObjectOfType<UIController>();
            Assert.IsNotNull(uiController);
            pickablesManager = FindObjectOfType<PickablesManager>();
            terrainManager = FindObjectOfType<TerrainManager>();

            actionInstance = new IdleAction();
        }

        public void WorldUpdate()
        {
            ActionInstance.Act();
            if (ActionInstance.Progress >= 1.0f)
            {
                if (ActionQueue == null)
                {
                    ActionQueue = new ActionQueue(this);
                }

                if (ActionQueue.Count == 0)
                {
                    ActionInstance = new IdleAction();
                }
                else if (ActionInstance != ActionQueue.Peek())
                {
                    ActionInstance = ActionQueue.Peek();
                }
                else
                {
                    ActionQueue.Dequeue();
                    ActionInstance = ActionQueue.Count > 0 ? ActionQueue.Peek() : new IdleAction();
                }
            }
        }

        public void FollowPath(IList<Vector3> path)
        {
            float moveDistance = velocity;
            while (moveDistance > 0.0f && path.Any())
            {
                var nextPoint = path.First();
                var diff = nextPoint - transform.localPosition;
                diff.y = 0;
                if (diff.sqrMagnitude > float.Epsilon)
                {
                    transform.localRotation = Quaternion.LookRotation(diff);
                }

                float dist = Vector3.Distance(nextPoint, transform.localPosition);
                if (dist > moveDistance)
                {
                    transform.localPosition = Vector3.MoveTowards(transform.localPosition, nextPoint, moveDistance);
                }
                else
                {
                    transform.localPosition = nextPoint;
                    path.RemoveAt(0);
                }

                moveDistance -= dist;
            }
        }

        private void SetAnimation(string animationName)
        {
            animator.Play(animationName);
        }

        public ActionQueue BuildOrder(UIOrderParams uiOrderParams)
        {
            var actions = new List<IActionInstance>();

            var targetPoint = uiOrderParams.Point;
            var target = uiOrderParams.TargetObject;
            if (target)
            {
                var pickup = target.GetComponent<Pickup>();
                if (pickup)
                {
                    targetPoint = pickup.transform.position;
                    actions.Add(new PickupAction(this, pickup));
                }

                var dropoff = target.GetComponent<Dropoff>();
                if (dropoff)
                {
                    targetPoint = dropoff.transform.position;
                    actions.Add(new DropoffAction(this, dropoff));
                }

            }

            var pathfindingResult = terrainManager.Pathfind(transform.position, targetPoint);
            var path = pathfindingResult.Path.ToList();

            if (target)
            {
                int firstOverDistance;
                for (firstOverDistance = path.Count - 1; firstOverDistance >= 0; firstOverDistance--)
                {
                    if (VectorMath.Distance2DSquared(path[firstOverDistance], targetPoint) > PickupRangeSquared)
                    {
                        break;
                    }
                }

                path.RemoveRange(firstOverDistance + 1, path.Count - firstOverDistance - 1);
            }

            if (path.Count > 0)
            {
                actions.Insert(0, new MoveAction(this, path));
            }

            return new ActionQueue(this, actions);
        }
    }
}