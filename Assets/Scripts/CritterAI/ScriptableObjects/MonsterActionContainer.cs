﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.CritterAI.Actions;
using UnityEngine;
using UnityEngine.Assertions;

[CreateAssetMenu(fileName = "New MonsterActionContainer", menuName = "Monster Action List", order = 51)]
public class MonsterActionContainer : ScriptableObject
{
    [SerializeField]
    public MonsterAIGenericAction contained;
    
    public void OnAfterDeserialize()
    {
        contained.preconditions = StripDupes(contained.preconditions);
        contained.effects = StripDupes(contained.effects);
    }

    public void OnBeforeSerialize()
    {
        contained.preconditions = StripDupes(contained.preconditions);
        contained.effects = StripDupes(contained.effects);
    }

    private List<StringBoolPair> StripDupes(List<StringBoolPair> keyValueList)
    {
        var byKey = keyValueList.GroupBy(x => x.Key);
        if (byKey.Any(x => x.Count() > 1))
        {
            var dupeKeys = byKey.Where(x => x.Count() > 1).Select(x => x.Key).ToArray();
            var dupeKeyString = string.Join(", ", dupeKeys);
            Debug.LogError($"Duplicate precondition keys: {dupeKeyString}", this);
            return byKey.Select(x => x.First()).ToList();
        }

        return keyValueList;
    }
}
