﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;

public class MouseEventFallback : MonoBehaviour, IPointerClickHandler, IPointerDownHandler
{
    private new Camera camera;

    private IPointerClickHandler redirectClickHandler;

    private IPointerDownHandler redirectDownHandler;

    [SerializeField]
    private MonoBehaviour redirectTarget = null;

    [SerializeField]
    private float scaleMult = 1.0f;

    public void OnPointerClick(PointerEventData eventData)
    {
        redirectClickHandler.OnPointerClick(eventData);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        redirectDownHandler.OnPointerDown(eventData);
    }

    private void Awake()
    {
        camera = GetComponentInParent<Camera>();
        Assert.IsNotNull(camera);
        Assert.IsNotNull(redirectTarget);

        redirectClickHandler = redirectTarget.GetComponent<IPointerClickHandler>();
        redirectDownHandler = redirectTarget.GetComponent<IPointerDownHandler>();
        Assert.IsNotNull(redirectClickHandler);
        Assert.IsNotNull(redirectDownHandler);
    }
    
    private void Update()
    {
        if (!transform.hasChanged)
        {
            return;
        }

        Vector3[] frustumCorners = new Vector3[4];
        camera.CalculateFrustumCorners(new Rect(0, 0, 1, 1), camera.farClipPlane, Camera.MonoOrStereoscopicEye.Mono, frustumCorners);

        float width = Vector3.Distance(frustumCorners[0], frustumCorners[2]);
        float height = Vector3.Distance(frustumCorners[0], frustumCorners[1]);
        var oldScale = transform.localScale;
        var newScale = new Vector3(scaleMult * width, scaleMult * height, 1.0f);
        if (Vector3.Distance(oldScale, newScale) > float.Epsilon)
        {
            transform.localScale = newScale;
        }
    }
}
