﻿using System.Linq;
using System.Text;
using Assets.CritterAI;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class WritePosition : MonoBehaviour, IDisplaysStatblock
{
    private Text text;
    private Selection selection;

    public void DisplaySelection(Selection selection)
    {
        this.selection = selection;
    }

    private void Awake()
    {
        text = GetComponent<Text>();
    }

    private void Update()
    {
        if (selection == null)
        {
            text.text = "N/A";
            return;
        }

        var stringBuilder = new StringBuilder();
        foreach (var selected in selection)
        {
            Describe(stringBuilder, selected);
            stringBuilder.AppendLine();
        }

        text.text = stringBuilder.ToString();
    }

    private static void Describe(StringBuilder stringBuilder, SelectedItem selected)
    {
        stringBuilder.AppendLine(selected.Selectable.gameObject.ToString());
        stringBuilder.AppendLine(selected.Selectable.transform.position.ToString());

        var monster = selected.Selectable.GetComponent<Monster>();
        if (monster)
        {
            var actionQueue = monster.ActionQueue;
            if (actionQueue != null)
            {
                foreach (var action in actionQueue)
                {
                    stringBuilder.Append(action.ToString());
                    stringBuilder.Append(';');
                }

                stringBuilder.AppendLine();
            }
        }
    }
}
