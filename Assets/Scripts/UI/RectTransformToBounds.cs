﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UIElements;

public class RectTransformToBounds : MonoBehaviour
{
    private RectTransform rectTransform;

    private Renderer[] coveredRenderers;

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        Assert.IsNotNull(rectTransform);
        coveredRenderers = transform.parent.GetComponentsInChildren<Renderer>();
        Assert.IsTrue(coveredRenderers.Length > 0);
        Update();
    }

    private void Update()
    {
        if (!transform.hasChanged)
        {
            return;
        }

        transform.hasChanged = false;
        var bounds = coveredRenderers.First().bounds;
        foreach (var renderer in coveredRenderers)
        {
            bounds.Encapsulate(renderer.bounds);
        }

        bounds.center = transform.InverseTransformPoint(bounds.center);
        bounds.size = transform.InverseTransformVector(bounds.size);
        rectTransform.sizeDelta = bounds.size;
    }
}
