﻿using UnityEngine;

public interface ITracksObject
{
    GameObject Tracked { get; set; }
}