﻿using System;
using System.Collections.Generic;
using Assets.Actions;
using UnityEngine;
using UnityEngine.Assertions;

public class UIController : MonoBehaviour
{
    private IDisplaysStatblock[] statblockDisplayers;
    private Selection lastSelection;

    private Dictionary<GameObject, GameObject> progressBars = new Dictionary<GameObject, GameObject>();

    [SerializeField]
    private GameObject progressBarPrefab = null;

    public Canvas Canvas { get; private set; }

    private void Awake()
    {
        Canvas = GetComponentInChildren<Canvas>();
        statblockDisplayers = GetComponentsInChildren<IDisplaysStatblock>();
        DisplaySelection(lastSelection);
    }

    public void DisplaySelection(Selection selection)
    {
        lastSelection = selection;
        if (statblockDisplayers == null)
        {
            return;
        }

        foreach (var displayer in statblockDisplayers)
        {
            displayer.DisplaySelection(selection);
        }
    }

    public void AddActionProgress(GameObject actor, IActionInstance actionInstance)
    {
        var progressBarObject = GameObject.Instantiate(progressBarPrefab, Canvas.transform);
        var progressBar = progressBarObject.GetComponent<ActionProgressBar>();
        Assert.IsNotNull(progressBar);
        progressBar.ActionInstance = actionInstance;
        var centeringScript = progressBarObject.GetComponent<CenterOnGameObject>();
        centeringScript.Tracked = actor;

        progressBars[actor] = progressBarObject;
    }

    public void RemoveActionProgress(GameObject actor)
    {
        var progressBarObject = progressBars[actor];
        progressBars.Remove(actor);
        Assert.IsNotNull(progressBarObject);
        GameObject.Destroy(progressBarObject);
    }

    public void ShowActionProgress(GameObject actor, IActionInstance actionInstance)
    {
        if (progressBars.TryGetValue(actor, out GameObject oldBar))
        {
            GameObject.Destroy(oldBar);
        }

        AddActionProgress(actor, actionInstance);
    }
}