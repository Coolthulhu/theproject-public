﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Drawing;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;

public class MouseSelector : MonoBehaviour, IPointerClickHandler, IPointerDownHandler
{
    [SerializeField]
    private Camera cam = null;

    [SerializeField]
    private PreviewManager previewManager = null;

    [SerializeField]
    private Selector selector = null;
    
    [SerializeField]
    private UIController uiController = null;
    private PointerEventData lastPointerEventData = null;
    private bool lastShift = false;

    private Selection selection;
    public Selection Selection
    {
        get { return selection; }
        set {
            // TODO: Move to messaging system
            selection = value;
            uiController.DisplaySelection(selection);
            previewManager.SetSelection(selection);
        }
    }

    private void Awake()
    {
        Assert.IsNotNull(uiController);
        Assert.IsNotNull(previewManager);
        Selection = selector.Select(Array.Empty<Selectable>());
        lastPointerEventData = null;
    }

    public void Init()
    {
        Selection = selector.Select(FindObjectsOfType<Selectable>());
    }

    private void Update()
    {
        Vector3 mousePos = Input.mousePosition;
        Ray ray = cam.ScreenPointToRay(mousePos);
        Debug.DrawRay(ray.origin, ray.direction * 100, Color.yellow);

        var hitInfo = GetMouseIntersectionWithCollider();
        var targetObject = hitInfo.collider?.gameObject;
        previewManager.HandleMouseover(targetObject);
        if (lastPointerEventData != null && lastPointerEventData.button == PointerEventData.InputButton.Left)
        {
            var selectables = targetObject != null
                ? targetObject.GetComponents<Selectable>()
                : Array.Empty<Selectable>();
            if (lastShift)
            {
                Selection = selector.ToggleSelection(Selection, selectables);
            }
            else
            {
                Selection = selector.Select(selectables);
            }
        }

        var orderParams = new UIOrderParams(hitInfo.point, targetObject);
        var preparedOrders = Selection.BuildOrder(orderParams);
        previewManager.Preview(preparedOrders);
        if (lastPointerEventData != null && lastPointerEventData.button == PointerEventData.InputButton.Right)
        {
            foreach (var singleOrder in preparedOrders)
            {
                singleOrder.Apply();
            }
        }

        lastPointerEventData = null;
    }

    private RaycastHit GetMouseIntersectionWithCollider()
    {
        Ray ray = GetRay();
        var hits = Physics.RaycastAll(ray);
        // TODO: Lots of GetComponent here
        var commandable = hits.Where(raycastHit => raycastHit.collider != null)
            .Where(raycastHit => raycastHit.collider.GetComponent<Selectable>() != null);
        if (!commandable.Any())
        {
            var myCollider = GetComponent<Collider>();
            return hits.FirstOrDefault(x => x.collider == myCollider);
        }
        // TODO: That's weird math, should be distance in projection on screen
        return commandable.OrderBy(raycastHit => Vector3.Distance(raycastHit.transform.position, ray.origin)).FirstOrDefault();
    }

    private Ray GetRay()
    {
        Vector3 mousePos = Input.mousePosition;
        return cam.ScreenPointToRay(mousePos);
    }

    public Vector3 GetMouseIntersectionWithZeroY()
    {
        Ray ray = GetRay();
        new Plane(Vector3.up, Vector3.zero).Raycast(ray, out float dist);
        return ray.GetPoint(dist);
    }

    public void EventTest(string eventData)
    {
        Debug.Log(eventData);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        lastPointerEventData = eventData;
        lastShift = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
    }
}