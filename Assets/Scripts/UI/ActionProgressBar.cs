﻿using Assets.Actions;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class ActionProgressBar : MonoBehaviour
{
    private Slider slider;
    private Text text;
    private IActionInstance actionInstance;

    public IActionInstance ActionInstance
    {
        get => actionInstance;
        set
        {
            actionInstance = value;
            slider.value = actionInstance.Progress;
            text.text = actionInstance.Name;
        }
    }

    private void Awake()
    {
        slider = GetComponentInChildren<Slider>();
        text = GetComponentInChildren<Text>();
        Assert.IsNotNull(slider);
    }

    private void Update()
    {
        if (slider.value != ActionInstance.Progress)
        {
            slider.value = ActionInstance.Progress;
        }
    }
}