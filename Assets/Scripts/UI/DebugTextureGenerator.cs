﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

public static class DebugTextureGenerator
{
    public static Texture2D PathfindingTextureFromHeightmap(float[] hmap, int width, int height)
    {
        Contract.Requires<ArgumentNullException>(hmap != null);
        Ensure.NotNull(hmap);
        Assert.AreEqual(hmap.Length, width * height);
        var texture = new Texture2D(width, height, TextureFormat.ARGB32, false);

        for (int x = 1; x < width - 1; x++)
        {
            for (int y = 1; y < height - 1; y++)
            {
                var ul = hmap[x + y * width];
                var ur = hmap[x + 1 + y * width];
                var ll = hmap[x + (y + 1) * width];
                var lr = hmap[x + 1 + (y + 1) * width];
                float[] vals = {ul, ur, ll, lr};
                if (vals.Max() > vals.Min() + 1)
                {
                    texture.SetPixel(x, y, Color.red);
                }
                else
                {
                    texture.SetPixel(x, y, Color.green);
                }
            }
        }
 
        texture.Apply();
        texture.filterMode = FilterMode.Point;

        return texture;
    }

    internal static Texture PathfindingTextureFromCache(PathfindingCache pathfindingCache)
    {
        int width = pathfindingCache.W;
        int height = pathfindingCache.H;
        var hmap = pathfindingCache.Hmap;
        var costs = pathfindingCache.Costs;
        Assert.AreEqual(hmap.Count, width * height);
        var texture = new Texture2D(width, height, TextureFormat.ARGB32, false);

        float maxCost = costs.Max();

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                var cost = costs[x + y * width];
                if (cost < 0.0f)
                {
                    texture.SetPixel(x, y, Color.red);
                }
                else
                {
                    float r = cost / maxCost;
                    float g = Mathf.Max(1.0f - (maxCost - cost) / maxCost, 0.5f);
                    float b = (Mathf.RoundToInt(r * 100) % 10) / 10.0f;
                    texture.SetPixel(x, y, new Color(r, g, b));
                }
            }
        }

        texture.Apply();
        texture.filterMode = FilterMode.Point;

        return texture;
    }
}
