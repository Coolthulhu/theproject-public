﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.Assertions;

public class PopupFieldHacky<T> : PopupField<T>
{
    private readonly List<T> hackChoices;
    private readonly int defaultIndex;

    public PopupFieldHacky(List<T> choices, T defaultValue) : base(choices, defaultValue)
    {
        Assert.IsTrue(choices.Count > 0);
    }

    public PopupFieldHacky(List<T> choices, int defaultIndex) : base(choices, defaultIndex)
    {
        Assert.IsTrue(choices.Count > 0);
        hackChoices = choices;
        this.defaultIndex = defaultIndex;
        base.index = 0;
        base.value = choices[0];
    }

    public override T value
    {
        get => base.value;
        set
        {
            try
            {
                base.value = value;
            }
            catch (ArgumentException)
            {
                value = hackChoices[defaultIndex];
            }
        }
    }
}