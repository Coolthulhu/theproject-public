﻿// Based on:
// https://forum.unity.com/threads/propertydrawer-with-uielements-changes-in-array-dont-refresh-inspector.747467/#post-4985996

using System.Linq;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

using MakeItemsType = System.Func<UnityEditor.SerializedProperty, System.Collections.Generic.IEnumerable<UnityEngine.UIElements.VisualElement>>;

public class ArrayWrapper : BindableElement, INotifyValueChanged<int>
{
    private const string BoxName = "item-box";
    private const string RemoveButtonName = "remove-button";
    private readonly SerializedProperty serializedProperty;

    private MakeItemsType MakeItems { get; set; }

    public override VisualElement contentContainer => internalContainer;

    private readonly VisualElement internalContainer;

    private int targetArraySize = 0;

    public ArrayWrapper(SerializedProperty arrayProperty, MakeItemsType makeItems)
    {
        serializedProperty = arrayProperty;
        var header = new VisualElement();

        header.Add(new Label(serializedProperty.displayName));

        var addFirstButton = new Button(() =>
        {
            AddToArray(0);
        });
        addFirstButton.text = "+";
        header.Add(addFirstButton);
        header.style.flexDirection = FlexDirection.Row;
        header.style.justifyContent = Justify.SpaceBetween;

        var footer = new VisualElement();
        var addLastButton = new Button(() =>
        {
            AddToArray(serializedProperty.arraySize);
        });
        addLastButton.text = "+";
        footer.Add(addLastButton);
        footer.style.flexDirection = FlexDirection.Row;
        footer.style.justifyContent = Justify.SpaceBetween;
        
        // We use a content container so that array size = child count      
        // And the child management becomes easier
        internalContainer = new VisualElement() { name = "array-contents" };
        this.hierarchy.Add(header);
        this.hierarchy.Add(internalContainer);
        this.hierarchy.Add(footer);

        this.MakeItems = makeItems;

        var property = serializedProperty.Copy();
        var endProperty = property.GetEndProperty();

        //We prefill the container since we know we will need this
        property.NextVisible(true); // Expand the first child.
        do
        {
            if (SerializedProperty.EqualContents(property, endProperty))
            {
                break;
            }

            if (property.propertyType == SerializedPropertyType.ArraySize)
            {
                targetArraySize = property.intValue;
                bindingPath = property.propertyPath;
                break;
            }
        }
        while (property.NextVisible(false));

        UpdateUIItems();
    }

    private void AddToArray(int index)
    {
        serializedProperty.InsertArrayElementAtIndex(index);
        serializedProperty.serializedObject.ApplyModifiedProperties();
        value++;
    }

    private VisualElement WrapItem(int index, VisualElement visualElement)
    {
        var box = new Box
        {
            name = BoxName
        };

        // This should belong in uss
        box.style.flexDirection = FlexDirection.Row;
        box.style.justifyContent = new StyleEnum<Justify>(Justify.SpaceBetween);
        
        int indexCopyForClosure = index;
        var removeButton = new Button(() =>
        {
            serializedProperty.DeleteArrayElementAtIndex(indexCopyForClosure);
            serializedProperty.serializedObject.ApplyModifiedProperties();
            this.value--;
        })
        {
            name = RemoveButtonName,
            text = "-"
        };

        box.Add(visualElement);
        box.Add(removeButton);
        return box;
    }

    public void RebuildUIItems()
    {
        Clear();
        UpdateUIItems();
        this.Bind(serializedProperty.serializedObject);
    }

    private bool UpdateUIItems()
    {
        int lastSize = childCount;

        int targetSize = this.targetArraySize;

        for (int i = childCount - 1; i >= 0; i--)
        {
            RemoveAt(i);
        }

        this.Bind(serializedProperty.serializedObject);
        var newItems = MakeItems(serializedProperty.Copy()).ToArray();
        for (int i = 0; i < newItems.Length; i++)
        {
            Add(WrapItem(i, newItems[i]));
        }

        return true;
    }

    public void SetValueWithoutNotify(int newSize)
    {
        this.targetArraySize = newSize;

        UpdateArray();
        UpdateUIItems();
        
        this.Bind(serializedProperty.serializedObject);
    }

    private void UpdateArray()
    {
        int lastSize = serializedProperty.arraySize;
        int targetSize = this.targetArraySize;

        if (lastSize == targetSize)
        {
            return;
        }

        for (int i = lastSize - 1; i >= targetSize; --i)
        {
            serializedProperty.DeleteArrayElementAtIndex(i);
        }

        for (int i = lastSize; i < targetSize; ++i)
        {
            serializedProperty.InsertArrayElementAtIndex(i);
        }

        serializedProperty.serializedObject.ApplyModifiedProperties();
    }

    public int value
    {
        get => targetArraySize;
        set
        {
            if (targetArraySize == value)
            {
                return;
            }

            if (panel != null)
            {
                using (var evt = ChangeEvent<int>.GetPooled(targetArraySize, value))
                {
                    evt.target = this;

                    // The order is important here: we want to update the value, then send the event,
                    // so the binding writes and updates the serialized object
                    targetArraySize = value;
                    SendEvent(evt);

                    //Then we remove or create + bind the needed items
                    SetValueWithoutNotify(value);
                }
            }
            else
            {
                SetValueWithoutNotify(value);
            }
        }
    }
}

