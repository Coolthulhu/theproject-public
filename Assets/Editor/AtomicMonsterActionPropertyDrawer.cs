﻿/// Based on https://medium.com/@trepala.aleksander/serializereference-in-unity-b4ee10274f48

using Assets.Scripts.CritterAI.Actions;
using System;
using System.Linq;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.Assertions;
using UnityEngine.UIElements;

[CustomPropertyDrawer(typeof(AtomicMonsterAction))]
public class AtomicMonsterActionPropertyDrawer : PropertyDrawer
{
    private Type[] implementations;
    
    public override VisualElement CreatePropertyGUI(SerializedProperty property)
    {
        implementations = GetImplementations(typeof(AtomicMonsterAction));

        var root = new VisualElement();

        // Hacky
        bool isNull = property.managedReferenceFullTypename.Length == 0;
        if (isNull)
        {
            property.managedReferenceValue = Activator.CreateInstance(implementations[0]);
            property.serializedObject.ApplyModifiedProperties();
        }

        var typeName = property.managedReferenceFullTypename.Split('.').Last();
        var currentType = implementations.First(x => x.Name == typeName);
        var typePicker = new PopupField<Type>(implementations.ToList(), currentType);
        typePicker.RegisterValueChangedCallback((evt) =>
        {
            property.managedReferenceValue = Activator.CreateInstance(evt.newValue);
            property.serializedObject.ApplyModifiedProperties();
        });
        root.Add(typePicker);

        return root;
    }

    public static Type[] GetImplementations(Type interfaceType)
    {
        var types = AppDomain.CurrentDomain.GetAssemblies().SelectMany(assembly => assembly.GetTypes());
        return types.Where(p =>
                interfaceType.IsAssignableFrom(p) && !p.IsAbstract && !p.IsSubclassOf(typeof(UnityEngine.Object)))
            .ToArray();
    }
}
