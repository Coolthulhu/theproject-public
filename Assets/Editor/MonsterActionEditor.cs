using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine.UIElements;
using UnityEditor.UIElements;
using UnityEngine.Assertions;
using Assets.Scripts.AI.Decisions;
using Assets.Utils;

[CustomEditor(typeof(MonsterActionContainer))]
public class MonsterActionEditor : Editor
{
    private const string FieldName = "prop-field";
    private const string KeyFieldName = "KeyFieldName";
    private const string ValueFieldName = "ValueFieldName";

    private static readonly string[] AcceptableKeys = typeof(MonsterAINode).GetTypedPropertyNames<bool>();

    private ArrayWrapper preconditionsWrapper;
    private ArrayWrapper effectsWrapper;
    private ArrayWrapper actionsWrapper;

    public override VisualElement CreateInspectorGUI()
    {
        var root = new VisualElement();

        preconditionsWrapper = MakeStringBoolPairWrapper("contained.preconditions", PreconditionsKeyChanged);
        effectsWrapper = MakeStringBoolPairWrapper("contained.effects", EffectsKeyChanged);

        var actionsProperty = serializedObject.FindProperty("contained.actions");
        Assert.IsNotNull(actionsProperty);
        actionsWrapper = new ArrayWrapper(actionsProperty, MakeActionItems);

        root.Add(preconditionsWrapper);
        root.Add(effectsWrapper);
        root.Add(actionsWrapper);

        return root;
    }

    private IEnumerable<VisualElement> MakeActionItems(SerializedProperty serializedProperty)
    {
        var arrayPropertyPath = serializedProperty.propertyPath;

        var items = new List<VisualElement>(serializedProperty.arraySize);
        for (int i = 0; i < serializedProperty.arraySize; i++)
        {
            var propertyPath = $"{arrayPropertyPath}.Array.data[{i}]";
            var element = new PropertyField();
            element.bindingPath = propertyPath;
            items.Add(element);
        }

        serializedProperty.serializedObject.ApplyModifiedProperties();

        return items;
    }

    private ArrayWrapper MakeStringBoolPairWrapper(string propertyPath, EventCallback<ChangeEvent<string>> onKeyFieldChanged)
    {
        var serializedProperty = serializedObject.FindProperty(propertyPath);
        Assert.IsNotNull(serializedProperty);
        return new ArrayWrapper(serializedProperty, x => MakeStringBoolPairItems(x, onKeyFieldChanged));
    }

    private IEnumerable<VisualElement> MakeStringBoolPairItems(SerializedProperty serializedProperty, EventCallback<ChangeEvent<string>> onKeyFieldChanged)
    {
        FixupKeys(serializedProperty, out var remainingKeys);

        var arrayPropertyPath = serializedProperty.propertyPath;
        for (int i = 0; i < serializedProperty.arraySize; i++)
        {
            var pair = serializedProperty.GetArrayElementAtIndex(i);
            var keyProperty = pair.FindPropertyRelative("key");
            var key = keyProperty.stringValue;

            var myAvailableKeys = remainingKeys.Prepend(key).ToList();

            var propertyPath = $"{arrayPropertyPath}.Array.data[{i}]";

            var element = new VisualElement();
            element.style.flexDirection = FlexDirection.Row;
            element.style.flexBasis = 1f;
            element.style.flexGrow = 1f;
            element.style.justifyContent = Justify.SpaceBetween;

            var keyField = new PopupFieldHacky<string>(myAvailableKeys, 0);
            keyField.name = KeyFieldName;
            keyField.bindingPath = $"{propertyPath}.key";
            keyField.RegisterValueChangedCallback(onKeyFieldChanged);
            keyField.style.flexGrow = 1f;
            element.Add(keyField);

            var valueField = new Toggle();
            valueField.name = ValueFieldName;
            valueField.bindingPath = $"{propertyPath}.value";
            element.Add(valueField);

            yield return element;
        }
    }

    private static void FixupKeys(SerializedProperty serializedProperty, out List<string> remainingKeys)
    {
        remainingKeys = new List<string>(AcceptableKeys);
        var needFixup = new List<int>();
        for (int i = 0; i < serializedProperty.arraySize; i++)
        {
            var pair = serializedProperty.GetArrayElementAtIndex(i);
            var keyProperty = pair.FindPropertyRelative("key");
            var key = keyProperty.stringValue;
            if (remainingKeys.Contains(key))
            {
                remainingKeys.Remove(key);
            }
            else
            {
                needFixup.Add(i);
            }
        }

        needFixup.Reverse();
        int removeCount = needFixup.Count - remainingKeys.Count;
        foreach (var i in needFixup)
        {
            if (removeCount > 0)
            {
                serializedProperty.DeleteArrayElementAtIndex(i);
                removeCount--;
                continue;
            }

            var pair = serializedProperty.GetArrayElementAtIndex(i);
            var keyProperty = pair.FindPropertyRelative("key");
            var fixupKey = remainingKeys.First();
            keyProperty.stringValue = fixupKey;
            remainingKeys.Remove(fixupKey);
            pair.FindPropertyRelative("value").boolValue = false;
        }

        serializedProperty.serializedObject.ApplyModifiedProperties();
    }

    private void PreconditionsKeyChanged(ChangeEvent<string> evt)
    {
        preconditionsWrapper.schedule.Execute(() => preconditionsWrapper.RebuildUIItems());
    }

    private void EffectsKeyChanged(ChangeEvent<string> evt)
    {
        effectsWrapper.schedule.Execute(() => effectsWrapper.RebuildUIItems());
    }
}