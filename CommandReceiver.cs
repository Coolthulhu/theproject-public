﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CommandReceiver : MonoBehaviour
{
    [SerializeField]
    private bool autoPopulate = true;

    [SerializeField]
    private IList<ISelectable> receivers;
    
    void Start()
    {
        if (autoPopulate)
        {
            receivers = GetComponents<ISelectable>();
        }
    }

    
}
